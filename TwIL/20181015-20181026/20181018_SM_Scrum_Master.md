2018 년 10 월 18 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# Scrum Master

- Alias: Servant Leader

## Scrum 프로세스의 요소

- Product Backlog
- Sprint
- Sprint Planning Meeting
- Sprint Backlog
- Daily Scrum Meeting
- Dev
- Product Owner
  <!--  * 고객, 가치, 제품, 서비스 비전 제시 및 책임 -->
  <!--  * 백로그 정의 -->
  <!--  * 목표, Target(사람), 중요성, 제약요소 정의 -->
- Scrum Master

## 애자일 원칙

- 프로세스 Or 도구 < 구성원간 상호작용
- 포괄적인 문서 < 제대로 동작하는 소프트웨어
- 계획 준수 및 이행 < 변화에 대응
- 계약 & 협상 < 고객과의 협업

## Scrum master

- 프로젝트에서 구성원들 간 조율을 하고 문제를 해결하는 등의 역할을 하면서 업무를 진척시키는 관리자
- 애자일 개발 프로세스 행동 패턴을 코칭 & 유지
  - 애자일 프로세스를 도입 & 적용만으로는 유지되기 어렵기 때문에 경험이 있는 팀, 없는 팀 모두에 촉매로서 필요
- 지속적인 팀 & 제품 발전에 필요 (품질, 속도, 역량)
- Team 이 Scrum Goal 을 성취할 수 있도록 도움
  - 적절한 목표와 범위가 팀에 잘 전달될 수 있도록 공감대를 형성 및 동기부여
  - 의사결정을 돕고(하고 X), 의사소통의 윤활제
  - 일의 방해 요소를 제거

## Scrum Master`s To-Do

- Scrum Practice 교육
- 장애물 관리 (문제상황 인지 및 확대)
- 적합한 사람에게 적합한 업무가 전달될 수 있도록 함
- 팀 토론에 참여하여 facilitator 로서의 역할
- 효율적인 데모 미팅 기획
- 백로그 관리
- 팀과 Owner 사이의 협력을 도움
- 팀이 온전히 Sprint 에 집중할 수 있도록 도움

---

### Bonus track - 자격증?

- Certified Scrum Master
- Professional Scrum Master
- PMI Agile Certified Practitioner
- 등
