# Typescript in real-world

### Generate it!

```typescript
interface Sample {
  essential1: any;
  essential2: any;
  optional!: any;
}

type UpdateSampleObject = X<Sample> // 'essential1' | 'essential2';
```

### indexed access types(lookup type)

- interface 의 property type 을 참조할 수 있다.

```typescript
type Gender = "Male" | "Female";
type LastName = "철수" | "영이";
interface Human {
  readonly firstName: string;
  lastName: string;
  age: number;
}

const changeNameBad = (human: Human) => {
  const lastName: string = "아무개";
  return { ...human, lastName };
};

const changeNameGood = (human: Human) => {
  const lastName: Human["lastName"] = "아무개";
  return { ...human, lastName };
};
```

### keyof operator

- interface 의 key 들을 하나의 type 으로 translate 한다.

```typescript
type HumanKeys = keyof Human;

// is equal to
type HumanKeys = "firstName" | "lastName" | "age";
```

### Pick

```typescript
interface ABCD {
  a: any;
  b: any;
  c: any;
  d: any;
}

type BD = Pick<ABCD, "a" | "b">;

type Pick<T, K extends keyof T> = { [P in K]: T[P] };
```

### Exclude

```typescript
type ABCD = "a" | "b" | "c" | "d";
type BD = "b" | "d";

type AC = Exclude<ABCD, BD>;

type Exclude<T, U> = T extends U ? never : T;
```

### Omit ?

```typescript
interface ABCDEFG {
  a: any;
  b: any;
  c: any;
  d: any;
  e: any;
  f: any;
  g: any;
}
type BCDEFG = Pick<ABCD, "b" | "c" | "d" | "e" | "f" | "g">;

type BCDEFG = ?
```

### ?

```typescript
interface Sample {
  a: any;
  b: any;
  c: any;
}

const FFF = {};

const sampleTFF = {
  a: 1
};

const sampleFTF = {
  b: 1
};

const sampleFFT = {
  c: 1
};

const sampleTTF = {
  a: 1,
  b: 1
};

const sampleTFT = {
  a: 1,
  c: 1
};

const sampleFTT = {
  b: 1,
  c: 1
};

const smapleTTT = {
  a: 1,
  b: 1,
  c: 1
};
```

### update typing

```typescript
interface Human {
  readonly firstName: string;
  lastName: string;
  age: number;
}

class Teacher implements Human {

  readonly firstName: Human['firstName'];
  lastName: Human['lastName'];
  age: Human['age'];

  updateHuman = (humanData: ?) => {
    this.age = humanData.age;
    this.lastName = humanData.lastName;
  };
}
```

### mapped type modifiers can be added or removed

```typescript
//typescript 2.8
type MutableRequired<T> = { -readonly [P in keyof T]-?: T[P] };
type ReadonlyPartial<T> = { +readonly [P in keyof T]+?: T[P] };
```

http://www.typescriptlang.org/play/#src=type%20X%20%3D%20Exclude%3C%22a%22%20%7C%20%22b%22%2C%20%22b%22%3E%3B%0D%0Atype%20Omit%3CT%2C%20U%3E%20%3D%20%7B%20%5BP%20in%20Exclude%3Ckeyof%20T%2C%20U%3E%5D%3A%20T%5BP%5D%20%7D%3B%0D%0A%0D%0Ainterface%20Sample%20%7B%20%0D%0A%20%20a%3A%20any%2C%0D%0A%20%20b%3A%20any%2C%0D%0A%7D%0D%0A%0D%0Atype%20T%20%3D%20Omit%3CSample%2C%20%22a%22%20%7C%20%22b%22%3E%3B%0D%0A%0D%0Ainterface%20Human%20%7B%0D%0A%20%20readonly%20firstName%3A%20string%3B%0D%0A%20%20lastName%3A%20string%3B%0D%0A%20%20age%3A%20number%3B%0D%0A%7D%0D%0A%0D%0Aclass%20Teacher%20implements%20Human%20%7B%0D%0A%0D%0A%20%20readonly%20firstName%3A%20Human%5B'firstName'%5D%3B%0D%0A%20%20lastName%3A%20Human%5B'lastName'%5D%3B%0D%0A%20%20age%3A%20Human%5B'age'%5D%3B%0D%0A%0D%0A%20%20updateHuman%20%3D%20(humanData%3A%20Partial%3COmit%3CHuman%2C%20'firstName'%3E%3E)%20%3D%3E%20%7B%0D%0A%20%20%20%20this.age%20%3D%20humanData.age%3B%0D%0A%20%20%20%20this.lastName%20%3D%20humanData.lastName%3B%0D%0A%20%20%7D%3B%0D%0A%7D%0D%0A%0D%0Aconst%20teacher%20%3D%20new%20Teacher()%3B%0D%0Ateacher.updateHuman(%7B%20age%3A%201%20%7D)%3B%0D%0Ateacher.updateHuman(%7B%20lastName%3A%20'test'%20%7D)%3B%0D%0A%0D%0Aconsole.log(teacher)%3B
