# Apollo Server

## [Data sources](https://www.apollographql.com/docs/apollo-server/features/data-sources.html)

### Caching Partial Query Results

개별적인 서비스로 부터의 데이터 요청을 캡슐화하여 클래스로 만들어준다.

- 캐싱
- 중복 제거
- 에러 핸들링

```
npm install apollo-datasource-rest
```

RESTDataSource 를 상속하는 클래스를 만든다.

- GET, POST, PUT, PATCH, DELETE 요청을 만들 수 있다.

#### Intercepting fetches

- willSendRequest
  - 요청하기 전 헤더, 쿼리 파라미터 등등을 설정할 수 있다.

#### Resolving URLs dynamically

```
get baseURL() {
  if (this.context.env === 'development') {
    return 'https://movies-api-dev.example.com/';
  } else {
    return 'https://movies-api.example.com/';
  }
}
```

- 비동기로 주소를 가져와 설정 가능하다.

#### Accessing data sources from resolvers

ApolloServer 생성자에 아래와 같이 dataSources 속성에 클래스 생성하면 끝

```
new ApolloServer({
  dataSources: () => {
    return {
      moviesAPI: new MoviesAPI(()
    };
  },
  ...
});
```

- 모든 요청에 context 를 통해 dataSources 에 접근이 가능하다.

```
 Query: {
    movie: async (_source, { id }, { dataSources }) => {
      return dataSources.moviesAPI.getMovie(id);
    },
    ...
 }
```

#### What about DataLoader?

- https://www.apollographql.com/docs/apollo-server/features/data-sources.html#What-about-DataLoader
- Batching
  - 배치과 캐싱
    배치 요청을 하였을 시 요청했던 것들의 묶음이 완전히 일지 않은 이상 캐쉬를 제공받기 힘든 부분이 있다.

#### Using Memcached/Redis as a cache storage backend

- Memcached
- Redis

### Reference

- https://www.youtube.com/watch?v=qNXhhrJIv-w
- https://www.apollographql.com/docs/apollo-server/features/data-sources.html#What-about-DataLoader
