# Introduction

Huiseoul 플랫폼팀이 knowledge sharing 하는 방식에 대해서 설명합니다. 개인이 아닌 team 의 일원으로 일할 때의 가장 큰 장점은 바로 곁의 팀원들과 서로의 지식을 공유할 수 있다는 것입니다[1]. 함께 배워나가는 조직에 있는 것은 무언가를 배우기 위한 가장 좋은 방법입니다. 엔지니어는 끊임없이 배워야 하는 직업입니다. 엔지니어로서 살아남기 위해서, 나아가 살아남는 회사가 되기 위해서도 knowledge sharing 은 꼭 필요합니다. 그래서 huiseoul 의 플랫폼팀은 자율과 강제의 경계선에서 적극적으로 knowledge sharing 을 권장합니다.

## Content

### TwIL(This week I Learned)

- Who: 모든 엔지니어가 돌아가면서 한명씩
- What: 5분(max 15분)의 짧은 지식 공유
- When: Daily meeting 후
- Where: 자리 or 회의실
- How: 자유 형식
- Why: 간단한 지식들을 공유함으로써 팀과 자신의 지식을 늘림

### Huiseoul Geek's Talk

- Who: 모든 엔지니어가 돌아가면서 두명씩
- What: 20분(max 30분)의 지식 공유
- When: 한달에 한번
- Where: 회의실
- How: 외부 공개 conference 형식 (w. Beer & Pizza)
- Why: TwIL 보다 큰 단위의 지식을 외부에 공유함으로써 자신과 회사의 브랜드 가치를 높임
- 준비중

### Blog

- Who: 모든 엔지니어
- What: 블로그
- When: 언제든. 최소 한달에 한번.
- Where: [medium blog](https://engineering.huiseoul.com)
- How: 글을 잘 씀
- Why: TwIL 이나 geek's talk 등의 정보를 온라인에 공유함으로써 자신과 회사의 브랜드 가치를 높임

## Reference

[1] [Learning organization](https://en.wikipedia.org/wiki/Learning_organization)
