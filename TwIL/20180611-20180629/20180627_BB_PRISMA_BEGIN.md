# Primsa
어느 데이터베이스든 간에 Graphql 타입으로 정의하면 테이블 생성과 관계를 정의해준다. <br/>
현재는 MySQL, PostgreSQL 이용가능하다. <br/>
Client - Graphql Server - Prisma Server - DB 구조로 구성되게 된다.
따라서 Prisma Server가 DB와 직접 통신하고 Graphql Server는 Prisma와 Graphql로 통신하여 데이터를 전달받는다.<br/>

### Type 정의 (DB테이블 정의)
datamodel.graphql에 타입를 정의하면 된다.
```
type User {
    id: ID! @unique
    name: String!
    post: Post! @relation(name:"Hello")
    post2: Post! @relation(name:"Hello")
}

type Post {

}
....
```
User라는 타입을 정의한 후 prisma서버에 올린다. <br/>
```
prisma deploy 
```
테이블 생성과 graphql query, mutation를 자동생성해준다. (CRUD를 생성해준다.)
paging, filter, orderby등등..

#### System Field
```
id: ID! @unique
createdAt: DateTime!
updatedAt: DateTime!
```
해당 필드를 정의한다면 커스텀 필드 가질 수 없다.

### Tutorial
* [Tutorial](https://www.prisma.io/docs/tutorials/)

### Deployment
- Prisma Server를 올릴수 있는 https://app.prisma.io/를 제공해주어 deploy할 수 있다.
- [AWS Fargate](https://www.prisma.io/docs/tutorials/deploy-prisma-servers/aws-fargate-joofei3ahd)는 러닝커브가 있어 충분한 숙지후 이용할 수 있을거 같습니다...
