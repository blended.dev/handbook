# Apex

## Moto

> Build, deploy, and manage AWS Lambda functions with ease

![star](https://gitlab.com/huiseoul/handbook/uploads/f50e9276dcf427e98b4f9b7f70e2349d/image.png)

## Apex vs. Up

> Up is much better for **applications, apis, and sites**. Apex is more **function-oriented** like the Serverless.com framework, which can be better for pipelines and event processing. Up isn't actually coupled to Lambda / API Gateway at all, it could deploy to ECS, EC2, DO, Azure, etc.

- Up
  - Serverless webserver deployment
- Apex
  - Serverless functions deployment

## Structure

```
project.json
functions
├── bar
│   ├── function.json
│   └── index.js
└── foo
    ├── function.json
    └── index.js
```

## Process

- write function
- `apex deploy`

## Notes

- webpack 과 잘 어울릴 듯함
  - [apex-webpack-web3](https://github.com/johnwook/apex-webpack-web3)
- 성격에 맞게 functions 들을 모아서 하나의 repository 로 관리하면 좋을 듯함
  - e.g. https://gitlab.com/huiseoul/zhengzhou

