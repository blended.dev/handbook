# Schema first development

## As-is

- Graphql Schema
- TypeScript [Interface](https://gitlab.com/huiseoul/type)

## 아쉬운 것

- GraphQL schema 와 TypeScript interface 의 매칭
  - Repository 분리로 인한 물리적 거리
- Process 의 부재
- Serverless architecture 에 대한 mocking 및 simulation 이 어려움

## Schema first process

### Team

1. 팀이 모여서 해당 이슈 해결을 위해 필요한 interaction 정의
2. Interaction에 해당하는 graphql schema 정의
3. graphql schema에 해당하는 interface 정의
4. interface에 해당하는 example data 작성

### Backend

1. 예외 케이스 고려
2. mock server init

### Client

1. 위 명세 파악
2. mock server 를 바라보고 개발
