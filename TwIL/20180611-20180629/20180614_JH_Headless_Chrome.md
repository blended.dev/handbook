# Headless Chrome

> **a Frontend For your Frontend**

> **Headless Chrome = Chrome with out Chrome**

- `--headless --remote-debugging-port=9222`
  - 위 방법으로 접속했을경우 Chrome DevTools protocol 을 사용할 수 있습니다.
- 언제 사용하면 좋은가?
  - Testing
    - [GitLab](https://about.gitlab.com/2017/12/19/moving-to-headless-chrome/)
  - CI
    - [Travis CI](https://developers.google.com/web/updates/2017/06/headless-karma-mocha-chai)
  - Crawler
  - SSR

## [puppeteer](https://github.com/GoogleChrome/puppeteer)

#### Feature

![Imgur](https://i.imgur.com/XpmZWGT.png)

- Test all modern features
- Programmatic access to DevTools goodies

* `npm i puppeteer —save`
* Modern Node library for headless Chrome
* Easy to use
* Zero config
  - Bundles latest Chromium

### How to use

```js
const puppeteer = require("puppeteer");

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto("https://example.com");
  await page.screenshot({ path: "example.png" });

  await browser.close();
})();
```

- 장점

  - Pre-rendering JS sites

    - Easy ssr

      ```js
      async url => {
        const browser = await puppeteer.launch({ headless: true });
        const page = await browser.newPage();
        await page.goto(url, { waitUntil: "networkidle0" });
        const html = await page.content();
        await browser.close();
        return html;
      };
      ```

      ![Imgur](https://i.imgur.com/TBY86SJ.png)

    - Cache pre-rendered results


    ```js
     cons RENDER_CACHE=new Map();
     if(RENDER_CACHE.has(url) return RENDER_CACHE.get(url))
     // blabla
     RENDER_CACHE.set(url,html)
    ```

    - About non-DOM requests


    ````js
       await page.setRequestInterception(true);
       page.on('request',req=>{
       	const whiteList=['document','script','xhr','fetch']
       	if(!whiteList.includes(req.resourceType())){
           	return req.abort();
       	}
           req.continue()
       })
       ```

     - 이미지 같은 markup 이 아니라 호출하는 태그 같은경우 무시하고 랜더링합니다.
    ````

![Imgur](https://i.imgur.com/OIyQVKF.png)

### Reference

- https://developers.google.com/web/updates/2017/04/headless-chrome
- https://www.youtube.com/watch?v=lhZOFUY1weo
- https://chromium.googlesource.com/chromium/src/+/lkgr/headless/README.md
- https://codeburst.io/end-to-end-testing-with-headless-chrome-api-d564cb4150c3
- https://try-puppeteer.appspot.com/
- https://developers.google.com/web/tools/puppeteer/articles/ssr
