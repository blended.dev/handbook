# 17.08.06 TWIL

## 블록체인이란?

- 쉽게 말하면 P2P + Linked List
- 최초의 Genesis Block으로부터 Block을 계속 연결해 나가는 방식
- 비트 코인 등의 암호화폐의 경우, 블록체인을 "화폐"에 적용한 case!


아래는 Blockchain 구조로, Linked List와 같은 것을 알 수 있습니다

![Blockchain map](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Blockchain.svg/102px-Blockchain.svg.png)

## 발생 가능한 문제들
- 누구나 Block생성 가능한지?
  - 물론 누구나 생성 가능합니다
  - 다만 악의적인 목적으로 추가하는 경우가 있을 수 있음
  - 이러한 경우들을 배제할 수 있어야 함
- Linked List면 child가 여러개일 수 있지 않은지?
  - 물론 가능합니다...
  - 이러한 block을 stale block이라 부릅니다 (Ethereum에서는 Uncle Block)

=> 올바른 블록을 선택할 수 있는 **합의 알고리즘**이 필요합니다!

### Bizantine Fault Tolerance
![BFT](https://lisk.io/content/5-academy/2-blockchain-basics/4-how-does-blockchain-work/7-byzantine-fault-tolerance-explained/7a-byzantine-fault-tolerance.jpg)

- 300명의 병력이 있는 도시를 각각 100명의 병사를 가진 5명의 비잔틴 제국의 장군이 동시 공격
- 동시 공격하기 위해서 서로 메시지를 주고 받음
- 적 도시의 병력보다, 공격하려는 병사들이 더 많아야 함
- 하지만 5명의 장군 중에서 배신자가 있을 수 있고, 메시지를 변조할 수 있음
- 이러한 상황에서 어떻게 공격을 성공적으로 할 수 있을 것인가 하는 문제

### 증명 방식
- POW (Proof of Work): 작업 증명
  - Bitcoin, Ethereum
  - 노드를 추가하는 데 있어서 일을 했다는 것을 증명을 강제화함으로써 증명
  - 블록체인에서는 해시함수의 역을 계산해서 가장 먼저 계산한 사람이 가져가는 방식
  - BFT의 경우, 배신자가 받아들여지기 위해선  타 노드의 모든 작업분량에 대해서 다 위조를 해야함
  - 흔히 말하는 *채굴 방식*
  - 다만 들어가는 Resource가 굉장히 많아서, 다소 낭비라고 여겨지기도 함
- POS (Proof of Stake): 지분 증명
  - 일정 stake 이상 제시하고, 이중 알고리즘에 의해서 특정 블록이 선택됨
  - fork가 나뉘는 경우, 투표를 통해서 진행
  - BFT의 경우, 배신자가 조작된 내역을 넣었을 경우, 소유한 모든 코인을 제거함으로 문제 방지
  - EOS


## 블록체인의 구조

크게는 블록 header + 거래정보 + 기타 정보로 이루어져 있습니다
- 블록체인 특성을 완성하는 것은 블록 header
- 거래정보는 디지털 서명을 통해서 거래정보의 신뢰도를 높입니다

![Blockchain 구조](https://www.researchgate.net/profile/Khaled_Salah7/publication/321017113/figure/fig2/AS:614287290679309@1523468911795/Blockchain-design-structure-showing-chained-blocks-with-header-and-body-fields.png)

- version: 프로토콜 버전
- Difficulty Target: 난이도 조절용 수치
- Block Hash: 블록 헤더 전체를 해쉬함수로 변환한 값
- Merkle Root
- Nonce

### Merkle Root
![Merkle Tree Structure](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Hash_Tree.svg/2200px-Hash_Tree.svg.png)
Merkle Tree(= Hash Tree)의 Top Parent Node

- 노드의 이름이 자식 노드들의 이름으로 구성된 트리 구조를 일컬음
- 블록체인에서는 존재하는 모든 거래내역에 대해서 Merkle Tree 형식으로 저장 후, 이에대한 Merkle Root를 저장합니다
- 특정 거래를 찾기 쉬움, 빠름!

### Nonce
POW에서 쓰이는 개념으로, hash 함수 계산 결과값이 특정 값보다 낮아지게 하는 값

- 위에서 말한 Difficulty Target이 바로 특정 값을 지정하게 됩니다
- hash 함수에 있어서 유일한 input이 nonce로, 이를 변화시켜 원하는 값을 얻을 수 있습니다
- Ethereum에서 쓰이는 nonce의 개념
  - POW Nonce: 위에서 설명한 개념입니다~
  - Account Nonce: Transaction 사이의 순서를 유지시키기 위해서 도입한 개념으로, 보통 Account의 Transaction Count로 사용합니다

## 블록체인 해결할 과제
### 51% attack
과반수의 노드가 동시에 거래내역을 조작하는 행위
초기 코인에서 일어날 수 있는 attack

### Double Spending
![DoubleSpending](https://i.ytimg.com/vi/87bHzNsZs7M/maxresdefault.jpg)
동일한 암호화폐로 2번이상 지불되는 case
중복되는 Transaction이 있으면 다른 한쪽이 안받아들여지는 것을 이용






