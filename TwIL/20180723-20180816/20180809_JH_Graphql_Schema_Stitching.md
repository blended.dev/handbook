# Graphql schema stitching

- Graphql schema stitching은 여러개의 graphql schema를 한개의 schema로 합치는 방법입니다.

  

### 기존의 문제점

- graphql은 하나의 endpoint 를 사용하는 것을 기본으로 합니다. 
- 하지만 Schema 가 커지면서 문제점이 생기게 됩니다.
  1. 단일 repo에서 schema를 관리하기 어려움이 있습니다.
  2. 만약 schema에서 한 query를 업데이트를 할 경우 배포 Plan을 진행하면서 다른 schema에 영향을 주게 됩니다.

![](https://cdn-images-1.medium.com/max/1600/1*abI3_s-HJJ0cLrT5UM1rfQ.png)

### 장점

- Graphql의 확장성 증가 
  - 많은 사람들이 사용한 Schema를 공유하고 병합하여 사용할 수 있습니다.
- Versioning, testing, deploying 을 부분적으로 실행 할 수 있습니다
- Open API와 결합이 쉽습니다.

### 단점

- 모든 Graphql 을 관리하는 것이 어렵습니다.
- Batch Request를 효율적으로 관리하기 어렵습니다.
- Schema를 나누는 것은 쉽지 않습니다.
- Schema를 중복해서 정의하거나 Type이 중복할 수 있습니다.

![](https://cdn-images-1.medium.com/max/1288/1*bnJDUtrr4_wLjt_jtvRaFA.png)

DEMO

```typescript
import * as express from "express";
import * as bodyParser from "body-parser";
import { graphqlExpress, graphiqlExpress } from "apollo-server-express";
import {
  makeRemoteExecutableSchema,
  mergeSchemas,
  introspectSchema
} from "graphql-tools";
import { createApolloFetch } from "apollo-fetch";

const run = async () => {
  const createRemoteSchema = async (uri: string) => {
    const fetcher = createApolloFetch({ uri });
    return makeRemoteExecutableSchema({
      schema: await introspectSchema(fetcher),
      fetcher
    });
  };
  const universeSchema = await createRemoteSchema(
    "https://www.universe.com/graphql/beta"
  );
  const weatherSchema = await createRemoteSchema(
    "https://5rrx10z19.lp.gql.zone/graphql"
  );
  const linkSchemaDefs = `
		extend type Location {
			weather: Weather
		}
		
    extend type Event {
        location: Location
		}
  `;
  const schema = mergeSchemas({
    schemas: [universeSchema, weatherSchema, linkSchemaDefs],
    resolvers: mergeInfo => ({
      Event: {
        location: {
          fragment: `fragment EventFragment on Event {cityName}`,
          resolve(parent: any, args: any, context: any, info: any) {
            const place: string = parent.cityName;
            return mergeInfo.delegate(
              "query",
              "location",
              { place },
              context,
              info
            );
          }
        }
      }
    })
  });

  const app = express();

  app.use("/graphql", bodyParser.json(), graphqlExpress({ schema }));

  app.use(
    "/graphiql",
    graphiqlExpress({
      endpointURL: "/graphql",
      query: `query {
  event(id: "5983706debf3140039d1e8b4") {
    title
    description
    url
    location {
      city
      country
      weather {
        summary
        temperature
      }
    }
  }
}
      `
    })
  );

  app.listen(3000);
  console.log(
    "Server running. Open http://localhost:3000/graphiql to run queries."
  );
};

try {
  run();
} catch (e) {
  console.log(e, e.message, e.stack);
}

```











## REFERENCE

https://blog.apollographql.com/graphql-schema-stitching-8af23354ac37

https://blog.apollographql.com/the-next-generation-of-schema-stitching-2716b3b259c0

https://www.prisma.io/blog/graphql-schema-stitching-explained-schema-delegation-4c6caf468405/

https://www.prisma.io/blog/how-do-graphql-remote-schemas-work-7118237c89d7/

https://www.apollographql.com/docs/graphql-tools/schema-stitching.html

