# Migrating to Apollo server v2.0

- Building on 1.x and **fully backwards compatible**
- 2.0’s defaults bake in the **best practices** and patterns gathered from two years of community feedback and iteration. 

## Code change

```typescript
const apolloServer = new ApolloServer({
  cacheControl: true,
  context: ({ req }: { req: IncomingMessage }) => makeContext(req),
  engine: false,
  resolvers: resolvers as any,
  tracing: true,
  typeDefs
});
```

- remove schema

## Automatic Persisted Queries

- graphql query 가 string 으로 그 크기가 크므로, query 에 대한 id 를 생성

![apq](https://cdn-images-1.medium.com/max/800/1*w5vsEwpB-Bx0ORZxqcWQdA.png)

- client 에서는 [apollo-link-persistent-queries](https://github.com/apollographql/apollo-link-persisted-queries) 를 사용해야 함

## CDN integration

- add cache hints to the GrahpQL schema

## GraphQL errors

- code 가 포함된 custom error throw

```typescript
const { ApolloError, ForbiddenError, AuthenticationError } = require("apollo-server");
```

## Performance monitoring

- engine is enabled by default
- Proxy 없이 전송 가능하다고 함
  - CacheCogntrol 에 대한 기존 store option 을 대체하는 코드를 아직 못찾았습니다.

## File Uploads

- [문서 링크](https://www.apollographql.com/docs/apollo-server/v2/whats-new.html#File-Uploads)
- 된다고 하는데, 사용할 예정이 없어서 큰 관심은 없음.

## Subscriptions

- [문서 링크](https://www.apollographql.com/docs/apollo-server/v2/whats-new.html#Subscriptions)
- 된다고 하는데, 사용할 예정이 없어서 큰 관심은 없음.

## Data sources

> Data sources are classes that encapsulate fetching data from a particular service, with built-in support for caching, deduplication, and error handling. You write the code that is specific to interacting with your backend, and Apollo Server takes care of the rest.

- Dataloader 의 확장으로 볼 수 있을 듯 합니다.
- Batching, caching 을 같이 가능하게 함.
