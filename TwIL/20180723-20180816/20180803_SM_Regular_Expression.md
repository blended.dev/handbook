2018 년 8 월 3 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# Regular Expression (정규식, 정규 표현식)

## 정규식?

- 특정한 규칙을 가진 문자열의 집합을 표현하는 형식 언어
  - POSIX: 정규표현식 표준
  - PCRE: 표준 확장형
- Meta 문자: 정규식에서 사용하는 기호
- 아래에 이어지는 실습은 여기서! https://regexr.com/

---

## 규칙 1 - basic

- `/` 로 감싸보자
  - `/gray/`
- `|`: 또는 `(우선순위)`: 연산자 범위 및 우선순위
  - `/gray|grey/` === `/gr(e|a)y/`

---

## 규칙 2 - 반복

- 문자 뒤에 붙여보자
- `?`: 0 or 1 회
  - `/colou?r/` => colour, color (O), colouuuur (X)
- `*`: 0 번 이상
  - `/colou*r/` => colour, color, colouuuur (O)
- `+`: 1 번 이상
  - `/colo+u?r/` => colour, color, coloooor, coloooour
- `{n}`, `{min, }`, `{min, max}`
  - `/co{2}l/` => cool
  - `/co{2,}l/` => cool, coool, cooool, cooool, ...
  - `/co{2, 3}l/` => cool, coool

---

## 규칙 3 - 클래스

- `.`: 1 개 문자와 일치
  - `/.ool/` => cool, pool
- `[문자]`: [] 안의 문자중 하나를 선택 -> `|`를 여러번 쓴것과 같음
- `[^문자]`: [] 안의 문자를 제외한 나머지 선택
  - `-`로 범위 지정 가능
  - ex) `/[cool]/` === `/[col]/`
  - ex) `/[a-zA-Z0-9가-힣]/` -> 모든 문자
  - ex) `/[^0-9]/` -> 숫자 제외

---

## 규칙 4 - 예약 패턴

- `\d`: 숫자
- `\w`: 영문 및 `_`
- `\s`: 공백 (스페이스, 탭 등)
  > 대문자로 쓰면 역!
- `\D`: 숫자 제외
- `\W`: 영문 및 `_` 와 제외
- `\S`: 공백 (스페이스, 탭 등) 제외

---

## 규칙 5 - 위치

- `^`: 처음
  - `/^Reg\w+/`: Regular, RegExp
- `$`: 끝
  - `\w+\.$`: hello., itsMe.
- `\n`: 일치하는 n 번째 패턴

---
