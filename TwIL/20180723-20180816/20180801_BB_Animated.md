
# Animated
간단한 리액트 네이티브 애니메이션에 대해 소개하고자 합니다.
리액트, 리액트 네이티브는 [animatedjs](https://github.com/animatedjs/animated) 동작방식을 따릅니다.
Animated 사용법에 대해선 간단하게 http://animatedjs.github.io/interactive-docs/ 에서 실행하여 볼 수 있습니다.
- timing vs spring
  - timing은 해당 toValue만큼 정직하게 이동하고 멈춥니다.
  - spring은 toValue만큼 이동하면서 시각적인 효과를 주기 위해 toValue보다 조금더 움직이다 toValue로 돌아옵니다.
  - 어떤걸 쓰느냐는 조금씩 다르겠지만 주로 spring을 쓰는것을 추천하고 있습니다.
- interpolate
  - 하나의 애니메이션이 아닌 유기적으로 하나의 toValue값이 바뀌면 그에 대응하여 컬러나 opacity등등을 바꿀 수 있게 해주는 역할을 합니다.
```
- inputRange: scale, outputRange: rotate
this.state.anim.interpolate({
      inputRange: [0, 1],
      outputRange: ['260deg', '0deg']
    });
```

- stopAnimation vs Interrupt Animations
  - stopAnimation은 callback을 받아 직접 컨트롤 할 수 있지만 Interrupt Animations은 사용자가 컨트롤할 수 없다.

- Animation Chaining
```
Animated.spring(anims[0], {toValue: anims[1]}).start();
Animated.spring(anims[1], {toValue: anims[2]}).start();
Animated.spring(anims[2], {toValue: anims[3]}).start();
```
- parallel
  - animation을 병렬로 동시에 실행할 수 있다.
- sequence
  - 순차적인 실행

나머지는 [Example](https://github.com/BoBinLee/animated-example)와 http://animatedjs.github.io/interactive-docs/를 참조하여 보면 된다.

### Example
- https://github.com/BoBinLee/animated-example

### Reference
- http://animatedjs.github.io/interactive-docs/

