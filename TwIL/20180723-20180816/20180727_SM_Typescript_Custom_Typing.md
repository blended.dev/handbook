2018 년 7 월 27 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# TypeScript - Custom Typings

## TypeScript Type

- TypeScript 는 기본적으로 node_modules >> installed library > `index.d.ts`를 참조하여 Type 을 확인한다.
- `package.json`에 dependency 를 추가하면 두 가지 방법으로 TypeScript type 을 확인할 수 있다.

  - 1.  설치된 library 가 이미 지원하고 있는 TypeScript Type

    - node_modules.mobx-state-tree directory

      ![MST-Typing](https://cdn-images-1.medium.com/max/600/1*Z6T6fQoLDdE-G3kZsEp1nw.png)

    - mst index.d.ts

      ![MST-d.ts](https://cdn-images-1.medium.com/max/600/1*aKaye7_KM8jpQaOAk4M0-A.png)

  - 2.  [DefinitelyTyped](https://definitelytyped.org/) 커뮤니티에서 제공하는 Type

    - 커뮤니티에 올라온 typing 정보가 있을 경우 아래 명령어로 type 정의를 받을 수 있다.

    ```
    $ npm install --save-dev @types/{library}
    ```

    - node_modules.react directory

      ![React-Typing](https://cdn-images-1.medium.com/max/600/1*flkwE52tHHbdyLjrxITwbg.png)

    - react index.d.ts

      ![React-d.ts](https://cdn-images-1.medium.com/max/600/1*3YUHGnMa2zI5A_MlzAF8wg.png)

## type 정의가 없을 때

- library 에도 `@types`에도 type 정의가 없다!

```
Could not find a declaration file for module 'lambda-log'. '/Users/.../node_modules/lambda-log/index.js' implicitly has an 'any' type.
  Try `npm install @types/lambda-log` if it exists or add a new declaration (.d.ts) file containing `declare module 'lambda-log';`
```

![there is not declaration](https://cdn-images-1.medium.com/max/800/1*L2UsbliZ-K9xLVD3cUFmpg.png)

- 친절하게 `@types`로 설치해보라고 하지만 그래도 없다.

- Solution: 직접 Type 정의 파일을 만들어준다.
  - [tsconfig.json](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html) 의 `typeRoots` 설정 변경과 함께 TypeScript 공식문서의 가이드를 따른다.

### Custom Type 정의 방법

- [tsconfig.json](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html) 의 `typeRoots` 가이드 대로 해보자.

  - `root > typings > library-name` 경로에 `index.d.ts`파일을 생성한다

    ![directory](https://cdn-images-1.medium.com/max/1000/1*JgDdJ1V_Ff3FJG9bFSLaXg.png)

  - file 내용은 아래처럼

```typescript
// declare module "library-name"
declare module "lambda-log" {

}
```

- error 메시지가 바뀌었다.

  - 처음에는 해당 라이브러리의 타입 정보가 없다고 했지만, 이번에는 코드에서 쓰고자 하는 속성이 없다고 한다.

  ```
  Property 'options' does not exist on type 'typeof import("lambda-log")'.
  ```

  ![error](https://cdn-images-1.medium.com/max/1000/1*xbopba-qZCCSFTCmmgX-Vg.png)

- 라이브러리의 index.js 파일을 참고하여 커스텀 타입 정의 파일을 채운다.

```typescript
declare module "lambda-log" {
  function log(level: string, message: string, meta?: object, tags?: string[]);

  // ...

  declare const options: options;
  interface options {
    meta: any;
    tags: string[];
    dynamicMeta: any;
    debug: boolean;
    dev: boolean;
    silent: boolean;
    replacer: any;
    stdoutStream: any;
    stderrStream: any;
  }
  export { log, /* ..., */ options };
}
```

- 끝으로 `tsconfig.json`에 아래처럼 추가한다.
  - 2.9 번대 버전에서는 아래 두 경로 모두 default 이므로 변경하지 않아도 된다.

```
// tsconfig.json

{
  "compilerOptions": {
    ...
    "typeRoots": ["./typings", "./node_modules/@types"]
  }
}
```
