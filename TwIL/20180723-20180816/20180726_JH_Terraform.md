

# Terraform



- Terraform은 Code 로 Infrastructure를 관리하기 윈한 도구입니다. -> IaS
  - ![](http://blog.outsider.ne.kr/attach/1/8459637210.gif)
- Terraform 은 Provision 의 단계를 가지고 있습니다.
  - [Provision 과 deploy의 차이](http://codefol.io/posts/deployment-versus-provisioning-versus-orchestration/)
- .tf 의 파일 포멧을 사용하고 있지만  .tf.json으로도 관리할 수 있습니다.
- `brew install terraform` 으로 설치할 수 있습니다.
- ` terraform init` 를 하면 terraform 환경을 구축합니다.
- ` terrafrom plan` 을 하면 실행시 변경점 을 먼저 확인할 수 있습니다.
- `terraform apply` 를 통해 Plan 에서 나왔던 변경점을 실행 시킬 수 있습니다.



### Terraform 의 사용처

- Terraform 은 serverless Framework 와는 다르게 Serverless 와 관련된 서비스 뿐만 아니라 EC2같은 다양한 서비스를 관리 할때 유용한 도구입니다.

example dynamoDB setting

```
provider "aws" {
  profile = "jinho"
  region  = "ap-northeast-2"
}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "GameScores"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "UserId"
  range_key      = "GameTitle"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "GameTitle"
    type = "S"
  }

  attribute {
    name = "TopScore"
    type = "N"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  global_secondary_index {
    name               = "GameTitleIndex"
    hash_key           = "GameTitle"
    range_key          = "TopScore"
    write_capacity     = 1
    read_capacity      = 1
    projection_type    = "INCLUDE"
    non_key_attributes = ["UserId"]
  }

  tags {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}

```

example ec2

```
provider "aws" {
  profile = "jinho"
  region  = "ap-northeast-2"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"

  tags {
    Name = "HelloWorld"
  }
}
```



example Lambda

```
provider "aws" {
  profile = "jinho"
  region  = "${var.aws_region}"
}

data "archive_file" "zip" {
  type        = "zip"
  source_file = "hello_lambda.js"
  output_path = "hello_lambda.zip"
}

data "aws_iam_policy_document" "policy" {
  statement {
    sid    = ""
    effect = "Allow"

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = "${data.aws_iam_policy_document.policy.json}"
}

resource "aws_lambda_function" "lambda" {
  function_name = "hello_lambda"

  filename         = "${data.archive_file.zip.output_path}"
  source_code_hash = "${data.archive_file.zip.output_sha}"

  role    = "${aws_iam_role.iam_for_lambda.arn}"
  handler = "hello_lambda.lambda_handler"
  runtime = "nodejs8.10"

  environment {
    variables = {
      greeting = "Hello"
    }
  }
}

```

```
# variables.tf
output "lambda" {
  value = "${aws_lambda_function.lambda.qualified_arn}"
}

```

```
# outputs.tf
variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "ap-northeast-2"
}

```

