#AppSync

## 기존의 모바일 환경에서의 고민

- 모바일의 제한
- 한 계정의 다수의 기기
- 인증 방법 -> cognito
- 데이터 동기화 -> 데이터 충돌시 해결법
- 다양한 플랫폼
- 효율적인 네트워크 -> 불안정한 네트워크에서 해결법
- 실시간 데이터
- Offline 에서도 비지니스 진행
- 분할된 데이터를 관리하는 방법

## AppSync

![Image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS6MvVLFcmSLjjSdK_HlKjiaNTD6TjkY-v8YrsJZZgItE_YgqZ3YA)

- 인프라가 아니라 어플리케이션에 집중하기위해서 만들어짐
- 클라우드와 모바일 의 데이터를 실시간으로 관리
- 오프라인 일때 데이터를 사용 가능
- 관리형 Graphql 서비스
- Cloud 에서 충돌처리 시 처리 가능

## AppSync 의 Resouce

![Imgur](https://i.imgur.com/lW6F09Q.png)

- DynamoDB
- Elasticsearch
- Lambda
- Http
  를 설정할 수 있습니다.
  각각 의 Resouce 를 cli 형태로 관리할 수 있습니다.

## AppSync Authorization

![Imgur](https://i.imgur.com/JqoDdSf.png)

- API Key 를 선택할 경우 Key 가 들어있는 Json 을 Export 해서 사용합니다.
