# Infrastructure of apollo server review

## As-is infrastructure of apllo server

![image](https://gitlab.com/cosmochain/graphql/uploads/e1c343a13e4e172b721afb28de4ac37e/image.png)

## Role of infrastructure

![image](https://gitlab.com/cosmochain/graphql/uploads/2f7b28545c61097271662057a2d65c8b/image.png)

## Function of infrastructure

![image](https://gitlab.com/cosmochain/graphql/uploads/b4b0a969b36f6b5f8634fc50cc8961bb/image.png)

## Newly added function of apllo server v2

![image](https://gitlab.com/cosmochain/graphql/uploads/1a373f971d575a09650f2a834c8d19d3/image.png)

- Apollo server v2 에서 reporting 이 가능해짐
- Caching 의 일부 기능을 proxy 없이 구현 가능함(이번 주 안에 다시 정리해서 공유하겠습니다.)

## New infrastructure of apollo server

![image](https://gitlab.com/cosmochain/graphql/uploads/5fa11d61fb91fca2277ab8b2432dfac8/image.png)

- Pros: (A lot) less infrastructure cost
- Cons: (A bit) loss of caching flexibility

## To do

- Caching strategy 를 포함한 architecture 의 변경을 이번주 안에 공유
