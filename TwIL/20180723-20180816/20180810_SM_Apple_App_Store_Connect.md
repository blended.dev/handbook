2018 년 8 월 10 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# [App Store Connect](https://appstoreconnect.apple.com/) iOS 앱 등록

- App Store 에 앱을 등록하려면 App Store Connect 라는 산을 넘어야 한다.
- 예전에는 iTunes Connect 라고 불렀다.
- 문서를 봐도 어떻게 하라는건지 잘 모르겠는 게 특징이다.

## App 등록 절차

- 개발 > 앱 등록 > TestFlight > 심사제출 > 승인 또는 거절 > 거절 시 피드백 반영 후 재 제출

## 앱 등록?

- [App Store Connect](https://appstoreconnect.apple.com/) 접속 > MyApps 로 이동
  ![MyApps](https://cdn-images-1.medium.com/max/2000/1*7Rbat1Q6Gu29vfY4mfgTdw.png)
- New App 선택 후 앱 등록
  ![NewApp](https://cdn-images-1.medium.com/max/2000/1*YXLFUjUOpuKR6dnYdnkoMg.png)
  ![RegisterApp](https://cdn-images-1.medium.com/max/2000/1*bVb2cCauFiOr6pXckk92mQ.png)
- 다른건 그럭저럭 입력하겠는데 Bundle ID? 심지어 drop box. 직접 입력하는게 아니라는 의미.
- 등록을 잠시 멈추고 [developer.apple](https://developer.apple.com/)로 이동

## Certificates, Identifiers & Profiles

- [developer.apple](https://developer.apple.com/) > Account > Certificates, Identifiers & Profiles 로 이동
- 애플은 생각보다 친절했다. 우리는 여기서 세 가지만 해주면 된다.
- Certificate 등록 (자격/인증서 등록)
  ![certificate](https://cdn-images-1.medium.com/max/2000/1*mZkZI6IcEqvLpvqqPNEYnw.png)
- Identifier 등록 (App ID = Bundle ID 등록)
  ![identifier](https://cdn-images-1.medium.com/max/2000/1*wqDv8X0YNGUrqkDK8rJkyA.png)
  ![id_detail](https://cdn-images-1.medium.com/max/2000/1*mwcmKzwDDH7AqjSSNEtzFA.png)
- Provisioning Profile 등록 (App + 자격 = 공급자 등록)
  ![profile](https://cdn-images-1.medium.com/max/2000/1*pKtSo7fttzoDBY9V6d6pfg.png)
  ![profile_detail](https://cdn-images-1.medium.com/max/2000/1*j9XXIR06JuxE4a4iZC4p7A.png)

## 마무리

- 앱 등록에 필요한 걸 모두 만들었으니, 다시 App Store Connect 로 돌아가 앱 등록을 마무리 하자.
- App 등록의 Bundle ID 를 펼쳐보면 추가한 App ID 가 보인다. 선택 후 create!
- Xcode 에서 추가한 Certificates 를 등록하고 Bundle ID 를 변경해주면 App 업로드 준비 완료!
- Xcode > [Product > Build] > [Product > Archive] > [Window > Organizer] > App 선택 > Upload to App Store
