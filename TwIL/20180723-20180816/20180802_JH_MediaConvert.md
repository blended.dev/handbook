# MediaConvert

- AWS Elemental Media Services 중 하나로 on Demand Video Convert 기능을 제공합니다
- 기본적으로 S3 에서 Object 를 받아 Convert 후 S3 에 저장하는 구조입니다.

![](https://media.licdn.com/mpr/mpr/gcrc/dms/image/C5612AQERWij0Hsf7Bg/article-cover_image-shrink_600_2000/0?e=1539216000&v=beta&t=RgVLTuUatRE9R1jCBUbhKuVcW7U1IC5O-lIoRctEk40)

## Job

- Input Group

  - Input 의 값으로 S3 의 Object 를 받을 수 있습니다.

- Out Group

  ![](https://docs.pallycon.com/images/mediaconvert-2.png)

  - 위 그림에서 Output Group 을 설정할 수 있습니다.
    - `File group` 은 MPEG-4 나 Image snap shot 처럼 파일로 변환하는 경우 선택하시면 됩니다.
    - `Apple HLS` 는 변환시 HLS 형식의 .m3u8 형식의 interface 파일과 .ts 의 chunk file 로 변환됩니다. (.m3u8 로 접속하시면 됩니다.)
  - NameModifier 를 통해 Convert 된 파일의 이름을 지정할 수 있습니다.
  - ThumbNail 제작시 File group 으로 선택 -> `Video codec` -> `Frame capture to JPEG` 으로 선택하면 됩니다.
    - 이때는 Audio Output 을 삭제 해야합니다.

### endpoint

- AWS Elemental MediaConvert > account 에서 확인할 수 있습니다. aws-sdk 로 생성 할 수 있지만 매번 생성할 경우 접근이 차단 될 수 있으니 사용시 주의 가 필요합니다.

- ```
  Important

  If you request your account endpoint programatically, only do so once in your application. Don't make a request to the public endpoint each time you make a request to AWS Elemental MediaConvert. Otherwise, you will hit the throttle maximum on the public API endpoint.
  ```

## Reference

https://github.com/aws-samples/aws-media-services-simple-vod-workflow
