# Workflow

## Who

- 플랫폼팀 엔지니어
- JW(월), SK(화), BB(수), JH(목), SM(금) 순
- 4월 30일이 첫 시작

## When

- Daily meeting 이 끝난 후
- 5분 (최대 15분)

## What

- 한 주 동안 배운 것중 팀원들과 공유하고 싶은 것

## How

- 자유 형식

## Where

- 편한 곳

## Exceptions

- 휴일(공휴일, 연차 등)인 경우에는 패스합니다.
- 자신의 차례에 진행할 내용을 **하루 전** 데일리 미팅까지 공유해주세요.
  - e.g. 참조할 article, repository 등에 대한 link
- 자신의 차례에 진행하지 못할 경우 **하루 전** 데일리 미팅까지 이유와 함께 공유해주세요.
  - e.g. `이번주에는 새로 배운 게 없습니다(?)`, `내일 연차에요..`

## Publish

- [Publishing TwIL 문서](3_Publishing_TwIL.md) 참조
