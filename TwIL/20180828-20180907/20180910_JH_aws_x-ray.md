# AWS X-Ray

## 시작하기 전에

- BackEnd 서비스에 Apollo Engine 을 사용하고 사용자들의 지표를 볼 수 있었습니다.
  - 굉장히 좋았습니다.
    - Error 가 언제/어디서/얼마나/왜 발생하는지 알 수 있었습니다.
    - 사용자 지표를 볼 수 있었습니다.
    - 시각적으로 그래프로 볼 수 있었습니다.
- 하지만 Graphql 과 관련된 모니터링은 되었지만 그 외 Lambda 들은 모니터링이 안 되었습니다.
  - 보상 로직을 짤 때 정상적으로 작동 되는 것 인지 알기 힘들었습니다.
  - 오류가 발생했을 경우 어떠한 로직에서 문제가 발생한 것인지 알기 힘들 었습니다.
- 다수의 Lambda 를 사용하면서 구조가 복잡해 져서 Lambda 간의 연결성을 기록으로 남기고 싶어 졌습니다.

## AWS X-Ray

![Imgur](https://i.imgur.com/D5uDYaN.png)

- 장점

  - AWS Source 와 연계가 가능합니다.
  - 쉽고, 싸다
  - 좋은 시각화 서비스를 제공합니다.

- 단점
  - 다양한 Source 지원이 안됩니다.
    ![Imgur](https://i.imgur.com/lR2PwHV.png)
  - typescript 지원 X
  - 최대 저장 기간 (30 일)
  - 최대 저장 용량 (50MB)
  - 모니터링 도구라고 하기엔 기능이 부족합니다.

### 구조

![Imgur](https://i.imgur.com/gXwMQKi.png)

## DEMO

[GITHUB](https://github.com/jinhokong/aws-x-ray-example)

## 결론

- 현재 우리 서비스에 적용하면 좋은 서비스입니다.
- 사용하기 매우 쉬습니다.
- 좋은 모니터링 서비스이지만 기능이 적어서 다른 모니터링/로그 시스템을 같이 구축하는 편입니다.
- Typescript 미지원은 치명적인 단점입니다.
