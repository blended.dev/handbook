2018 년 08 월 30 일 [SunKim](https://medium.com/@seeart.sun) TwIL

## GraphQL project structure

### 1. As-Is

- 달리느라 정신 없어서 정신 없어진 graphql project 찬찬히 살펴보자.

![as-is directory](https://cdn-images-1.medium.com/max/1600/1*Y6-apR_BI9EX2hoxb6NllA.png)

- as-is directory structure 의도.
  - 각 mutation & query 마다 type 을 별도로 관리
    - 해당 mutaion & query 에서 정말 사용하는 input/output 만 다루기 위해
    - optional 의 무분별한 사용을 막기위해
  - graphql & typescript type 을 분리하여 한데 모은다.

### 2. 개선 Point

#### mutation, query, type 이 모두 섞여있는 `graphqlTypes`, `typescriptTeypes`

- 그게 뭐 어때서?
  - navigator 만 봐서는 무엇을 위한 type 인지 알기 어려움

#### output 이 명칭만 다르고 알고보면 똑같다

- 코드 재사용에 용이하지 않음
- type 명칭의 의미가 퇴색됨
  - comment, post = postOutput?

#### front-end 가 생각보다 많은 정보를 원한다

- backend 의 세세한 내용을 모르는 front-end 입낭에서는 인터페이스 일관성이 필요함
  - 유사한 mutation & query 는 비슷한 정보를 돌려줄거라고 생각할 가능성이 높음
- update OR create 의 경우, 받은 정보를 다시 model 에 반영하고자 하는 needs 가 큼

#### GraphQL Type Resolver 활용 못하고 있음

- output type 이 통일되어 있지 않아 type resolver 쓰기 어려움
- resolver 의 dataloader 지옥
- comment > post > author 가 필요하다고 할 때 post 에 author 넣고, comment 에 post 만 넣어줘도 될걸 지금은 각각의 resolver 에서 개별적으로 풀어주고 있음

#### model 부재로 dynamo query, scan 등 코드 중복

- 우선 line 이 길어 읽기 부담스러움
- 점점 코드 읽기가 버거워짐
- 모듈화가 안되어 있어, param 의 조건들을 일일이 살펴서 의도를 파악해야함

### 3. 그래서?

콘텐츠 제한 작업 고려하면서 느꼈다!

recomment 가 comment 를 부르고 comment 가 post 를 부르고 post 가 author 를 부르고 author 는 restriction 을 부르고...

아! 다른건 됐고 Graphql type Resolver 먼저 해결하자.

#### To-Do 요정 김선미

1. 공통으로 쓸 수 있는 graphql Output 을 파악한다

- option1. ...Output 을 버리고 model type 으로 변경
- option2. alias 할 수 있는 library search

2. Author, Post 등을 type resolver 로 교체

우선 여기까지만 해둬도 편할 것 같다. 이 후 여유가 된다면 아래도 해보자

---

3. directory 구조 재정의

- query / mutation / type / enum

4. model 및 공통 함수 분리
