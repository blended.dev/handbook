## ComponentDidCatch

리액트 16 에 추가된 라이프 사이클 componentDidCatch 가 있습니다.

문법적인 오류가 있을경우 UI 를 랜더하지 않는 문제가 있습니다.

프로덕션 경험에서 사용자는 랜더되지 않은 화면을 보고 에러가 난것인지 로딩중인지

알지 못합니다.

해당 라이프 사이클과 데코레이터를 이용하면, 에러가 발생했을때 유저가 인지 할수있는

에러 상황을 보여 줄 수 있습니다.

```js
import React from "react";

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return this.props.children;
  }
}

export default WrappedComponent => props => (
  <ErrorBoundary>
    <WrappedComponent {...props} />
  </ErrorBoundary>
);
```

그리고 사용

```js
import React, { Component } from "react";
import withErrorBoundary from "./ErrorBoundary";

@withErrorBoundary
class Item extends Component {
  render() {
    return this.props.items.map(item => (
      <li key={item.key} className="item">
        {item.name}
      </li>
    ));
  }
}

class MainComponent extends Component {
  render() {
    return (
      <div>
        Items Render!
        <ul className="items">
          <Item />;
        </ul>
      </div>
    );
  }
}
```

문제가 되는 컴포넌트에 items 가 무조건 props 로 내려온다는 가정하에 map 을 호출 하고 있습니다.

메인컴포넌트에서 items porps 를 넘기고 있지 않다면, items 는 undefined 일 것 입니다.

undefined 타입에서 map 을 호출 할 수 없기 때문에 문법적인 에러가 발생하고,

에러바운더리에 라이프사이클인 `componentDidCatch` 에서 에러를 캐치하게 됩니다.

그리고 에러 상황에 맞는 UI 를 출력 하게 됩니다.

#### 출처

[리액트 공식문서](https://reactjs.org/blog/2017/07/26/error-handling-in-react-16.html)
