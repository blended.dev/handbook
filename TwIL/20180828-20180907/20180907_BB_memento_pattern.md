# Memento Pattern

## 소개

[메멘토 패턴](https://ko.wikipedia.org/wiki/%EB%A9%94%EB%A9%98%ED%86%A0_%ED%8C%A8%ED%84%B4)

VSCode 나 IDEA Intelij 툴, 에디터와 같은 곳에 undo, redo 기능을 재사용이 쉽게 구현하기 위한 디자인 패턴이다.

originator

- 내부 상태를 갖고 있는 객체

caretaker

- Undo, Redo 를 직접적으로 실행하는 요소

memento

- 객체 상태를 갖고 있는 요소?

https://www.youtube.com/watch?v=QXh1O-qD_IU

Example 구현과 같이 caretaker 가 reset, undo 를 직접 실행하고
Sum 컴포넌트에서 내부 상태를 갖고 있음과 메멘토를 저장하고 복원하는 부분이 구현되어있다. (originator)
Memento 는 state 의 상태에 대한 클래스를 정의하고 있다.

## Example

- https://codesandbox.io/s/zkxj8pnpnp
