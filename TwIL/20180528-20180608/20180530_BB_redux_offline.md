2018 년 5 월 30 일 @bobeenlee

# Introduction

모바일 유저가 많은 오늘날 인터넷 환경도 좋고 하지만 아직도 일부 지역이나 타지역엔 인터넷 속도가 느린 문제점이 있습니다. <br/>
오프라인 경험을 높이기 위해서 offline first로 개발 구현할 수도 있지만 오늘 소개하고자 하는 것은 심플한 오프라인 아키텍쳐 구조를 통한 간단한 보여주고자 합니다. <br/>
오프라인 기반 아키텍쳐 설계는 redux-offline이나 [관련블로그](https://hackernoon.com/introducing-redux-offline-offline-first-architecture-for-progressive-web-applications-and-react-68c5167ecfe0)를 통해 숙자할 수 있습니다. <br/>
redux-offline은 오프라인 모드를 위한 [여러 feature](https://github.com/redux-offline/redux-offline/blob/develop/docs/README.md)들을 제공해주고 있으므로 문서를 구독하여 직접 사용할 수 있습니다. <br/>
만일 그러하지 못하면 직접 구현하는 방법이 있습니다. <br/>
아래 MST기반 offline 기반 example을 첨부해두었습니다. <br/>
오프라인 기반으로 구현 할 시엔 필요한 요소 ex)retries, resilient, optimistic UI, actionQueue 등등 적용해 나가는 형식으로하면 될 것입니다. <br/>

# Example

* [react-native-offline-mst](https://github.com/BoBinLee/react-native-offline-mst)
  * MST 로 구현한 Offline Architecture 기반 예제입니다.
  * 기능 : retries, persist state, network-resilient API (outbox)
* [redux-offline](https://github.com/redux-offline/redux-offline/tree/develop/examples/web)

# 실사례

* Gmail, Slack, PWA 사용 서비스? 등등등...

# Reference

* https://hackernoon.com/introducing-redux-offline-offline-first-architecture-for-progressive-web-applications-and-react-68c5167ecfe0
* https://github.com/redux-offline
* http://github.hubspot.com/offline/docs/welcome/
