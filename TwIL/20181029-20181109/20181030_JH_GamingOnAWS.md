#Gaming on AWS

![Imgur](https://i.imgur.com/iYJdlgA.png)

![Imgur](https://i.imgur.com/AIGl3V2.jpg)

## 게임을 위한 AWS

- 김일호 SA
- Agenda
  - footprint
    - 157 service
  - multiplay architecture
    - latency
    - anti cheat
  - AI/ML

### GameLift

- like FPS
- session manage
- dedicated server
- Automatic dynamic server capacity
- flexible Matching making
- No downtime for updates
- cross platform

### GameSparks

- turn base
- service enable
- cloudcode
  - business logic

### Data Lake on AWS

![Imgur](https://i.imgur.com/vEaMIYJ.jpg)

![Imgur](https://i.imgur.com/PAIz0Xr.jpg)

- firehose
- DirectConnect
- SnowBall
- DMS? DNS?
- Athena
- QuickSight
- EMR
- RedShift

### AI/ML on AWS

- Polly
- Comprehend → filling
- alexa

## 데브시스터즈 데이터 레이크 구축 이야기

- 박주홍 데이터 분석 및 인프라 팀장 / 이준호 데이터 분석 및 BI 팀장

### Before

- 2014
  - S3 → spark → indexed log
- Json 으로 저장함

  - 그냥 검색하게 되면 너무 오래 걸림
    - index 을 함( id , type , createdAt ...)

- 다양한 요청
  - 특정 field 만 조회
  - 필요 로그 만 조회
  - 정보 스냅샷
  - spark 가 아니라 SQL 로 접근

### After

- 그래서 Data Lake 생성!

  - 도입 배경
    - 부서별 다양한 데이터 요청
    - 요청 데이터 활용을 위한 다양한 포맷
    - 데이터 계층화를 통한 데이터 활용성 증대
  - Parquet 활용
    ![Imgur](https://i.imgur.com/Xs98hCc.jpg)
    ![Imgur](https://i.imgur.com/B4oKnV6.jpg)

        - Columnar Storage Format
        - 특정 field 만 접근 가능
            - 데이터를 field 끼리 저장하기 때문에 가능
        - partition 지원
            - 자동으로 indexing 이 됨
        - spark dataframe 호환

- 2018
  ![Imgur](https://i.imgur.com/2plUKnp.jpg)
- kafka → spark→ s3(raw data) → spark → s3( analytics log ) → 각 부서 에 맞게 분석
  ![Imgur](https://i.imgur.com/Ukmx01E.jpg)

  ![Imgur](https://i.imgur.com/tRN0UxJ.jpg)
  ![Imgur](https://i.imgur.com/bk6ib3B.jpg)
  ![Imgur](https://i.imgur.com/8EwIcAU.jpg)
