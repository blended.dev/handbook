# React Hook and Recompose

## Recompose
로딩, fetch 등등 state로직과 메소드들의 재사용성을 높이기 위해 recompose를 사용합니다.
hoc로 가능하지만 recompose는 hoc를 구현하기 위한 코드를 줄여주고 여러 hoc기능들을 제공해줍니다.
ex) onlyUpdateForKeys, pure, lifecycle, ...
- https://github.com/acdlite/recompose/blob/master/docs/API.md 참조

## React Hook
16.7에 나온 기능으로 [motivation](https://reactjs.org/docs/hooks-intro.html#motivation)을 갖고 있습니다.
functional 컴포넌트에도 state관리를 가능하게 하고 재사용성 높은 코드를 작성할 수 있습니다.
ex) useFetch, useCounter, useToggle, ...

- 다만 클래스 컴포넌트는 hook이용이 불가능하다는 단점이 있습니다.

### React Hook, Recompose 비교

구현 예제는 React Hook, Recompose를 fetch 템플릿화하여 어떤 차이점이 있는지 알아봅니다.
- https://swapi.co/documentation#intro

#### React Hook
- https://reactjs.org/docs/hooks-overview.html <br/>
useLoading, useFetch
##### Example
- https://github.com/BoBinLee/react-hook-and-recompose/tree/hook-example

#### Recompose
- https://github.com/acdlite/recompose <br/>
withLoading, withFetch
##### Example
- https://github.com/BoBinLee/react-hook-and-recompose/tree/recompose-example

## 결론 

이전 버젼 or 클래스 컴포넌트는 recompose를 이용하여 state의 재사용성을 높일 수 있을 것이고 이후 버젼(16.7)은 functional component를 이용하여 state 재사용성을 높일 수 있는 코드를 작성할 수 있습니다.
