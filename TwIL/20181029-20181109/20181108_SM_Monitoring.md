2018 년 11 월 08 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# Monitor Your Precious System

## Monitoring?

IT 서비스 지속하고 비즈니스를 유지하기위해 필요한 수준까지 성능을 발휘할 수 있도록 보장하기 위해 IT 자원의 변화를 관찰하는 것

> - [Application performance monitoring](https://en.wikipedia.org/wiki/Application_performance_management), monitoring and management of performance and availability of software applications
> - [Business transaction monitoring](https://en.wikipedia.org/wiki/Business_transaction_management), managing information technology from a business transaction perspective
> - [Network monitoring](https://en.wikipedia.org/wiki/Network_monitoring), systems that constantly monitors a computer network for slow or failing components and that notifies the network administrator
> - [System monitoring](https://en.wikipedia.org/wiki/System_monitoring), a process within a distributed system for collecting and storing state data
> - [Website monitoring](https://en.wikipedia.org/wiki/Website_monitoring),the process of testing and verifying that end-users can interact with a website or web application as expected  
>   **참고** : wikipedia

---

## What we want to know

- 왜 모니터링 해야할까?
- 무엇을 관찰할까?
  - Microservice 모니터링은 Monolithic과 무엇이 다른가?
- 어떻게?
  - 어디서 무엇을 수집할까?
  - 모니터링 데이터 수집과 배포를 위해 어떤 툴을 쓸 수 있을까?

---

## Why?

- 모든 시스템은 실패한다.
  - 한쪽에서 실패 또는 성능저하가 모든곳에 영향을 줄 수 있다.
  - 작은 유격이 시스템 전체를 다운시키기도 한다.
  - 잠재적 서비스 운영 악화 요소를 파악할 수 있다.
- 모니터링 서비스는 그 자체로 시장이다.
- 성능개선에 용이하다
  - 주요 error 패턴을 확인할 수 있다.
  - ex) 특정 Lambda or 명령 실행시간이 점점 늘어난다.
- SLA(Service Level Agreement)를 유지할 수 있다.
  - SLA를 통해 내/외부적으로 정상이라고 판단하는 범위를 규정 가능
  - 그러나 모니터링 없이는 SLA가 지켜지는지 확인할 수 조차 없음

### In Cosmochain case

- graphql sever에 apollo engine을 붙인건 천만 다행
- 그러나 lambda는?
- DynamoDB capacity가 넘쳐서 throttle
  - 정작 엔지니어는 이 사실을 모르고 업무 담당자로부터 서비스 이상 문의를 받음
  - 부끄러웡! 🙈

---

## What?

- Latency (execution speed of tick)
- Count (Lambda Invocation & Concurruncy)
- Crash reporting
- Alert: dashboards, email, slack, mobile
- Deploying status

### 단, 조건이 있어

- 대시보드 형태로 나와야한다.
- 실패 뿐만 아니라 성능 저하 시에도 Alert가 필요하다.

### Example

- Application
  - 전체 시스템 명령 처리량
  - 유입 유저 수
  - ex) 평소 시간당 1,000 건의 명령을 수행하다 갑자기 500건밖에 처리를 못하면 이상한거다.
- Platform
  - 자주 사용되는 top 10 query
  - 가장 빠른 10% 실행시간
  - 가장 느린 10% 실행시간
  - 분당 요청 / 처리 수
  - 평균 응답시간
  - 각 서비스별 성공/실패 ratio
- System Event
  - Deploy
  - Down & Boot
  - Scaling Event
  - Configuration Updates
  - 기타 시스템 설정 변경
