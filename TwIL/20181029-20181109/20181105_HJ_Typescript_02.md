# Typescript in real-world 02

## Before Start

```typescript
const untypedGenerator = function*() {
  const value: any = yield 1; // value의 타입은 무엇일까?
  return value;
};
```

## Generator

- Generator 는 빠져나갔다가 나중에 다시 돌아올 수 있는 함수다.

```typescript
const myFirstGenerator = function*() {
  yield 1;
  yield 2;
  yield 3;
};

const x = myFirstGenerator();

console.log(x.next()); // { done: false, value: 1 }
console.log(x.next()); // { done: false, value: 2 }
console.log(x.next()); // { done: false, value: 3 }
console.log(x.next()); // { done: true, value: undefined }
```

```typescript
const confusingGenerator = function*() {
  const simpleValue1 = -3 + 2;
  const simpleValue2 = Math.abs(simpleValue1);
  const whoAreYou = yield 100; // => undefined;
  yield whoAreYou;
  return whoAreYou;
};

const x = confusingGenerator();

console.log(x.next()); // { done: false, value: 100 }
console.log(x.next()); // { done: false, value: undefined }
console.log(x.next()); // { done: true, value: undefined }
```

## Generator flow

- Generator 함수의 flow
- 0. `const myGen = generator();` // generator 함수의 실행 context 를 저장할 객체 생성
- 1. `const result1 = generator.next();` // { done: false, value: 100 }
- ... 일반적인 함수와 같은 flow
- 2. `const x = yield exp;` // return 은 우선 제외
- 3. yield exp => yield exp' => yield exp'' => ... => yield VALUE => ? // 다음 next() 호출까지 evaluation 을 멈춤
- 4. `result1 === { done: false, value: VALUE }`
- 5. `const result2 = generator.next(myArg);`
- 6. yield VALUE => myArg;
- 7. `const x = myArg`;
- 8. x 의 메모리에 myArg 를 저장

```typescript
const nowYouKnowGenerator_Maybe = function*() {
  const t = yield new Promise((resolve, reject) => resolve(1)); // t가 any인 것은 알겠다! 근데 잠시만...?
  return t;
};

const gen = nowYouKnowGenerator_Maybe();
console.log(gen.next()); // { done: , value: };
```

- 그럼 `yield exp`에서 exp 가 <strong>비동기 작업</strong>일 경우에 도대체 누가 이걸 실행시키는건가?

## HOF for generator

- Generator 를 하나의 비동기 함수처럼 작동하게 만들어주는 HOF 를 만들 수 있다.

```typescript
const asyncRandom = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(Math.random()), 3000);
  });
};

const myRandomGenerator = function*() {
  const value = yield asyncRandom();
  return value;
};

const generatorToAsync = (
  generator: (...args: any[]) => IterableIterator<any>
) => (...args: any[]) => {
  return new Promise(async (resolve, reject) => {
    const gen = generator(...args);
    let res = gen.next();
    while (!res.done) {
      const value = await res.value;
      res = gen.next(value);
    }

    resolve(res.value);
  });
};

const myFancyAsyncRandom = generatorToAsync(myRandomGenerator);

async function start() {
  const result = await myFancyAsyncRandom();
  console.log(result);
}

start();
```

- redux-saga 의 saga 나 MST 의 flow 가 위와 같은 역할을 남몰래 해주고 있었다... (감동)

```typescript
const fetchContentById = flow(function*(postId: string) {
  const variables = {
    postId
  };
  const response = yield getRoot(self).fetch(POST_BY_ID, variables); // flow 함수에서 getRoot(...)의 실행 결과를 feedback한다.
  const postJSON = _.get(response.data, ["post"], {});
  if (_.isEmpty(postJSON)) {
    return;
  }

  const user = _.get(postJSON, "author");
  yield upsertContent({
    author: user,
    ..._.omit(postJSON, "author")
  });
  yield (getRoot(self).relationshipStore as IRelationshipStore).isFollowing(
    postJSON.authorId
  );
});
```

## async/await VS generator yield

- 비동기 작업을 tracking 하거나 middleware 를 추가하기 용이하다.

```typescript
const generatorToAsync = (
  generator: (...args: any[]) => IterableIterator<any>
) => (...args: any[]) => {
  return new Promise(async (resolve, reject) => {
    const gen = generator(...args);
    let res = gen.next();
    while (!res.done) {
      /* My Logger */
      logger(res);
      const value = await res.value;
      res = gen.next(value);
    }

    resolve(res.value);
  });
};
```

## Generator typing

- 결국 generator 의 특성과 언어 수준에서의 지원이 없기 때문에 type inference 가 만만치 않다.
- 수동으로 Assertion 을 사용하여 타이핑하는 수 밖에 없고 그게 자연스러운 것이다.

```typescript
const asyncRandom = () => {
  return new Promise<number>((resolve, reject) => {
    setTimeout(() => resolve(Math.random()), 3000);
  });
};
// () => IterableIterator(number)
const myRandomGenerator = function*() {
  const value = yield asyncRandom(); // Promise<T> // T // HOF가 asysnc resolve를 실행하여 반환해준다는 가정 하에 다음과 같은 타이핑이 가능하다.
  return value;
};

// yield exp에서 exp를 동기/비동기로 구분한다고 할 때 일반적인 HOF는 다음과 같은 작업을 해준다.
// const result = yield exp;
// Sync: yield exp =>* yield value, HOF는 해당 value를 그대로 리턴한다.
// 따라서 typeof result === typeof exp;
// Async: HOF는 Promise를 resolve시킨 결과를 반환한다.
// 따라서 exp가 Promise<T>의 타입이라고 할 때, typeof result === T;
type Unpromisify<T> = T extends Promise<infer U> ? U : T;

const myRobustRandomGenerator = function*() {
  const value: Unpromisify<
    ReturnType<typeof asyncRandom>
  > = yield asyncRandom(); // HOF가 asysnc resolve를 실행하여 반환해준다는 가정 하에 다음과 같은 타이핑이 가능하다.
  return value;
};
```

```typescript
// mobx-state-tree flow typing
export declare function flow<R>(
  generator: () => IterableIterator<any>
): () => Promise<R>;
export declare function flow<A1>(
  generator: (a1: A1) => IterableIterator<any>
): (a1: A1) => Promise<any>;
export declare function flow<A1, A2>(
  generator: (a1: A1, a2: A2) => IterableIterator<any>
): (a1: A1, a2: A2) => Promise<any>;
export declare function flow<A1, A2, A3>(
  generator: (a1: A1, a2: A2, a3: A3) => IterableIterator<any>
): (a1: A1, a2: A2, a3: A3) => Promise<any>;
export declare function flow<A1, A2, A3, A4>(
  generator: (a1: A1, a2: A2, a3: A3, a4: A4) => IterableIterator<any>
): (a1: A1, a2: A2, a3: A3, a4: A4) => Promise<any>;
export declare function flow<A1, A2, A3, A4, A5>(
  generator: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) => IterableIterator<any>
): (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) => Promise<any>;
export declare function flow<A1, A2, A3, A4, A5, A6>(
  generator: (
    a1: A1,
    a2: A2,
    a3: A3,
    a4: A4,
    a5: A5,
    a6: A6
  ) => IterableIterator<any>
): (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) => Promise<any>;
export declare function flow<A1, A2, A3, A4, A5, A6, A7>(
  generator: (
    a1: A1,
    a2: A2,
    a3: A3,
    a4: A4,
    a5: A5,
    a6: A6,
    a7: A7
  ) => IterableIterator<any>
): (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) => Promise<any>;
export declare function flow<A1, A2, A3, A4, A5, A6, A7, A8>(
  generator: (
    a1: A1,
    a2: A2,
    a3: A3,
    a4: A4,
    a5: A5,
    a6: A6,
    a7: A7,
    a8: A8
  ) => IterableIterator<any>
): (
  a1: A1,
  a2: A2,
  a3: A3,
  a4: A4,
  a5: A5,
  a6: A6,
  a7: A7,
  a8: A8
) => Promise<any>;

// 이건 어떨까?
export declare function flow<R, T extends any[]>(
  generator: (...args: T) => IterableIterator<any>
): (...args: T) => Promise<R>;
```

## 한 번 확인해보자

```typescript
type Unpromisify<T> = T extends Promise<infer U> ? U : T;

const asyncRandom = () => {
  return new Promise<number>((resolve, reject) => {
    setTimeout(() => resolve(Math.random()), 3000);
  });
};

const myRandomGenerator = function*() {
  const value: Unpromisify<
    ReturnType<typeof asyncRandom>
  > = yield asyncRandom();
  return value;
};

const generatorToAsync = <R, T extends any[]>(
  generator: (...args: T) => IterableIterator<any>
) => (...args: T) => {
  return new Promise<R>(async (resolve, reject) => {
    const gen = generator(...args);
    let res = gen.next();
    while (!res.done) {
      const value = await res.value;
      res = gen.next(value);
    }

    resolve(res.value);
  });
};

const myFancyAsyncRandom = generatorToAsync<number, []>(myRandomGenerator);

async function start() {
  const result = await myFancyAsyncRandom();
  console.log(result);
}

start();
```

## Typescript 의 한계

- Generic 에 default type 이 없다는 전제 하에, Generic 의 일부분만 대입하는 것이 불가능하다.

```typescript
function makeArray<T, U>(a1: T, a2: U) {
  return [a1, a2];
}

function makeArrayRedundancy<R, T, U>(a1: T, a2: U) {
  return [a1, a2];
}

const myFancyAsyncRandom = generatorToAsync<number>(myRandomGenerator); // Error
const myFancyAsyncRandom = generatorToAsync<number, []>(myRandomGenerator); // Work
// myRandomGenerator의 args가 수십개라면...? 귀찮다!
```
