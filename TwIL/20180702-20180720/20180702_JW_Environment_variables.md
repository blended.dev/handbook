# How to set configuration

## Store config in the environment

in [12 factor](https://12factor.net/config),

> An app’s *config* is everything that is likely to vary between [deploys](https://12factor.net/codebase) (staging, production, developer environments, etc)
>
> **The twelve-factor app stores config in environment variables** (often shortened to *env vars* or *env*). Env vars are easy to change between deploys without changing any code.

- 원칙: 배포(staging, production, development)에 따라 달라지는 설정(config)은 환경 변수에 저장한다.

## Runtime vs Compile-time configuration

### Runtime

- Node.js 에서의 `process.env`

- 실제 코드가 동작하는 환경의 변수를 runtime에서 사용

  - ```javascript
    // test.js
    if (process.env.NODE_ENV === "development") {
        console.log("hi, this is development environment");
    }
    ```

  - 위 코드는 실행할 때 환경 변수에 dependent 하게 동작

  - `NODE_ENV=development node test.js` => `hi, this is development envioronment`

### Compile-time

- Babel with [babel-plugin-transform-inline-environment-variables](https://babeljs.io/docs/en/babel-plugin-transform-inline-environment-variables/) or webpack with [environment plugin](https://webpack.js.org/plugins/environment-plugin/)
- 환경 변수 코드를 compile time에서 사용가능한 변수로 **치환**
  - 위 예제에서 test.js를 compile 하면 환경변수 값에 따라 output이 없을 수도 있음.

### 예제

- [environment-variables-test repo](https://github.com/johnwook/environment-variables-test)
