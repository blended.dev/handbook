# Dynamodb Data Mapper



## 7가지 package의 조합으로 이루어져 있습니다.

- [Amazon DynamoDB Automarshaller](https://github.com/awslabs/dynamodb-data-mapper-js/blob/master/packages/dynamodb-auto-marshaller),[Amazon DynamoDB Data Marshaller](https://github.com/awslabs/dynamodb-data-mapper-js/blob/master/packages/dynamodb-data-marshaller)
  - JS로된 값을 DynamoDB에 Type을 적용시키는 package
- [Amazon DynamoDB Batch Iterator](https://github.com/awslabs/dynamodb-data-mapper-js/blob/master/packages/dynamodb-batch-iterator)
  - 임의의 크기의 batch 연산을 지원하는 package
  - for await of 사용
- [Amazon DynamoDB DataMapper](https://github.com/awslabs/dynamodb-data-mapper-js/blob/master/packages/dynamodb-data-mapper)
  - Class 형식으로 DynamoDB를 사용할 수 있도록 하는 package
- [Amazon DynamoDB DataMapper Annotations](https://github.com/awslabs/dynamodb-data-mapper-js/blob/master/packages/dynamodb-data-mapper-annotations)
  - [Amazon DynamoDB DataMapper](https://github.com/awslabs/dynamodb-data-mapper-js/blob/master/packages/dynamodb-data-mapper) 에서 사용한 클래스를 기반으로 Annotation을 사용할 수있도록하는 package
- [Amazon DynamoDB Expressions](https://github.com/awslabs/dynamodb-data-mapper-js/blob/master/packages/dynamodb-expressions)
  - 사용자가 쓰기 쉽도록 표현을 추상화 합니다 ( Or, Not )
- [Amazon DynamoDB Query Iterator](https://github.com/awslabs/dynamodb-data-mapper-js/blob/master/packages/dynamodb-query-iterator)
  - async iterables를 사용하여 쿼리 또는 스캔 작업에서 반환된 모든 DynamoDB를 자동으로 반복하는 package

----

## Concept

* Typescript로 한가지 모델을 만들어서 재사용을 합니다
* annotation을 이용하여 쉽게 테이블 구성을 합니다.