2018 년 7 월 20 일 [SunKim](https://medium.com/@seeart.sun) TwIL

---

# Monitoring For MicroService

- 왜 해야할까?
- Monolithic 과 무엇이 다른가?
- 어디서 무엇을 수집해야하나?
- 모니터링 데이터 수집과 배포를 위해 어떤 툴을 쓸 수 있을까?

---

## Why

- 모든 시스템은 실패한다.
- 시스템 정상 여부를 판단할 기준이 필요하다.
  - ex) SLA(Service Level Agreement)
- 성능 개선 포인트를 알 수 있다.
- 주요 error 패턴을 확인할 수 있다.
  - 보완 -> 안정성 강화
- [방증] APM(Application Performance Monitoring)은 그 자체로 시장이다.

---

## What - 01

- Monitoring = gathering & reporting
- Short-term (for emergency & alert) data
- Long-term (analytics for pattern) data

---

## What - 02

- Application Metrics
- Platform Metrics
  - Top 10 query
  - 최단 실행시간 job
  - 최장 실행히산 job
  - 분당 요청 / 처리 수
  - 평균 응답시간
  - 각 서비스별 성공/실패 ratio
- System Event
  - deploy 상태 - 가장 많은 system fail point
  - scaling event
  - configuration updates

---

## How

- 대시보드 형태로 나와야 한다.
- low-level 퍼포먼스를 확인할 수 있어야 한다.
- 실패 및 성능 저하 시 Alert 받을 수 있어야 한다.
  - dashboards, email, slcak, mobile, ...
