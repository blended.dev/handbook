# styled-system
css in js의 디자인 시스템을 위한 유틸리티이다. <br/>
[styled-components](https://github.com/styled-components/styled-components)를 이용한다면 아래와 같은 코드를 작성하는데에 익숙할 것이다.
```
const Div = styled.div`
    padding: ${props => props.padding};
    margin: ${props => props.margin};
    ...
`;
```
웹 환경에서 반응형으로 작성하게 된다면 margin, padding값들이 화면크기에 따라 바뀌어야할 것이다. <br/>
그럴 경우 점점 코드가 복잡해지고 화면 크기에 따라서 margin, padding값 관리가 필요해질 것이다.<br/>
styled-system은 [반응형 props를 받아 지정할 수 있고](https://github.com/jxnblk/styled-system#responsive-style-props) [테마를 이용](https://github.com/jxnblk/styled-system/blob/master/docs/theming.md)하여 [1,2,3]이런식으로 테마에 지정한 값을 불러와 적용할 수 있다.<br/>
반응형 구현이 지정한 space, fontSize, margin, padding... 값을 이용하여 손쉽게 바꿀 수 있고 재지정할 수 있다.

### Article
활용 방안 위주로 소개해주는 글
- https://varun.ca/styled-system/

### Reference
- https://github.com/jxnblk/styled-system
- https://varun.ca/styled-system/
