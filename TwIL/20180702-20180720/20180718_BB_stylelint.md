# Stylelint

typescript엔 tslint, javascript엔 eslint가 있다면 css엔 stylelint가 존재한다. <br/>
css, sass, less, styled-component 스타일 검사를 지원하여 문법 에러를 사전에 잡을 수 있다.

#### styled-components 설치 가이드
- https://www.styled-components.com/docs/tooling#setup

#### typescript-styled-plugin
만일 typescript를 이용한다면 ts만의 플러그인이 존재한다.
- https://github.com/Microsoft/typescript-styled-plugin

### Example
RN 프로젝트에 stylelint를 적용한 샘플입니다. <br/>
유의해야할 점 https://github.com/stylelint/stylelint/issues/2987
- https://github.com/BoBinLee/stylelint-example

### Reference
- https://github.com/stylelint/stylelint
- https://github.com/styled-components/stylelint-processor-styled-components
