# 요약

- Mock server 를 이용해서 개발을 해봅시다.

## Background

![directory](https://gitlab.com/huiseoul/handbook/uploads/8e91c2520cd8a36a0267c5df5bff89c1/image.png)

![microservices](https://gitlab.com/huiseoul/handbook/uploads/25eb5d540026026d44669bd08ac6f52b/image.png)

- Client 개발 환경 구성하려면 microservices 를 모두..?
- Staging server 를 바라보면 되지만, client 개발 하면서 여러 가지 조건을 조작하면서 확인해야하는 경우에 대한 대응이 어려움

## Mock server

- Backend architecture 를 mocking 하는 server 를 만들자.
  - 어떤 input 이 있을 때 output 이 나온다는 것만 지키면 되겠다.

### Configuration

- webpack configuration

```javascript
module.exports = env =>
  merge(common, {
    devtool: "inline-source-map",
    mode: "development",
    output: {
      filename: "server.dev.js",
      library: "",
      libraryTarget: "commonjs2"
    },
    resolve: {
      alias: (env || {}).mock
        ? {
            "src/resolvers": "src/mockResolvers"
          }
        : {}
    },
    watch: true,
    watchOptions: {
      ignored: [/node_modules/, /dist/]
    }
  });
```

- package.json

```json
"scripts": {
    "build": "webpack ---config webpack.prod.js",
    "build:dev": "webpack ---config webpack.dev.js",
    "build:mock": "webpack ---config webpack.dev.js --env.mock",
  ...
}
```

### To do

- 시나리오를 갖춘 data set 만들기
  - Data set 을 넘어 behavior test 기준이 되어야 함
