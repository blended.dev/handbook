# lambda@edge 와 CloudWatch 로 온 디멘드 이미지 리사이즈 만들기

---



![](https://d2908q01vomqb2.cloudfront.net/5b384ce32d8cdef02bc3a139d4cac0a22bb029e8/2018/02/01/1.png)

### Lambda@edge 란?

 한 리전에서 함수를 작성한 후 최종 사용자에게 가까운 전 세계 AWS 리전에서 서버를 프로비저닝하거나 관리하지 않고 그러한 함수를 실행할 수 있습니다.

### Lambda@edge 제한사항

https://docs.aws.amazon.com/ko_kr/AmazonCloudFront/latest/DeveloperGuide/cloudfront-limits.html#limits-lambda-at-edge

https://docs.aws.amazon.com/ko_kr/AmazonCloudFront/latest/DeveloperGuide/lambda-requirements-limits.html

- 버젼을 선언해야 합니다. 단 별칭은 사용할 수 없습니다.
- us-east-1 으로 리전만 허용합니다.
- 다른 계정의 CloudFront에 사용할 수 없습니다.
- 권한 설정을 해야합니다.
- VPC와 연결할 수 없습니다
- 환경변수를 사용 할 수 없습니다
- Dead Letter Queue를 사용할 수 없습니다.
- Viewer Lambda는 메모리 128 mb, timeout 5초,  response size 40 kb, Lambda size 1mb의 제한이 있습니다.

### Lambda@edge 사용방법

```js
const request = event.Records[0].cf.request; // Request info

const response = event.Records[0].cf.response; // Response info

```



### CloudFront 설정

- `Origin Domain Name`:  **S3 Bucket**을 지정합니다.
- `Restrict Bucket Access`: Yes를 선택합니다.
  - S3 버킷에 CloudFront 만 접근할 수 있도록 설정하는 옵션입니다
- `Origin Access Identity`:  **Create a New Identity**를 선택합니다.
- `Grant Read Permissions on Bucket`: **Yes, Update Bucket Policy**을 선택합니다.
  - 자동으로 Bucket의 Policy를 설정 
- `Query String Forwarding and Caching`: **Forward all, cache based on whitelist**을 선택합니다.
  - 캐싱 기준으로 쿼리 문자열 파라미터 지정  [참고](https://docs.aws.amazon.com/ko_kr/AmazonCloudFront/latest/DeveloperGuide/QueryStringParameters.html)
- `Query String Whitelist`: **d**를 입력합니다.
- `Compress Objects Automatically`: **Yes**를 선택합니다.
  - 콘텐츠를 압축합니다.
  - `콘텐츠가 압축되면 파일 크기가 더 작아지므로— 다운로드 속도가 빨라집니다. 원래 크기의 1/4 이하로 줄어드는 경우도 있습니다.` 라고 합니다.

### Lambda@edge 권한 설정

#### Permissions

* lambda:GetFunction
* lambda:EnableReplication*
* iam:CreateServiceLinkedRole
* cloudfront:UpdateDistribution or cloudfront:CreateDistribution

#### Trust Relationship

```json
{
   "Version": "2012-10-17",
   "Statement": [
      {
         "Effect": "Allow",
         "Principal": {
            "Service": [
               "lambda.amazonaws.com",
               "edgelambda.amazonaws.com"
            ]
         },
         "Action": "sts:AssumeRole"
      }
   ]
}
```





## REFERENCE

- http://pyrasis.com/book/TheArtOfAmazonWebServices/Chapter12/02
- https://docs.aws.amazon.com/ko_kr/AmazonCloudFront/latest/DeveloperGuide/distribution-web-creating-console.html
- https://docs.aws.amazon.com/ko_kr/AmazonCloudFront/latest/DeveloperGuide/ServingCompressedFiles.html
- https://docs.aws.amazon.com/ko_kr/AmazonCloudFront/latest/DeveloperGuide/QueryStringParameters.html
- https://blog.wisen.co.kr/?p=7258