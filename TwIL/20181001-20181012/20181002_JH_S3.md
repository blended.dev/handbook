# S3 And More

## S3 란

- 매우 견고한 storage
  - 9 가 11 개 들어가는 견고함
  - S3 는 지리적으로 분리된 최소 3 개의 가용영역 (AZ) 에서 데이터 배포
    - 그래서 일부 AZ 가 작동하지 않거나 소실되어도 문제가 없습니다.

## S3 LifeCycle

![](https://image.slidesharecdn.com/srv301-latest-updates-best-p-64448123-f452-4f71-80a0-88baba7fe4d8-892712098-180405201301/95/srv301-latest-updates-best-practices-for-amazon-s3-5-638.jpg?cb=1522959198)

- S3 standard

  - 일반적인 저장
  - 99.9999999% 의 내구성
  - 낮은 가격
  - 빠른 속도

- S3 Standard IA

  - 최소 사이즈 128KB

- S3 OneZone IA

[azure is dead!](https://azure.microsoft.com/en-us/status/history/)

![](https://lh4.googleusercontent.com/BLSybpGCwok4Z-MgO5rzwH1WuQZLNGm4wvH81HDSgTxZqvMwnC5NbJCpxfCc8MFahhw-aW3mrSKuhQPC3D3agb7V2vvh-w-JpLQpwL8VTHcP7hdwqiVSq8-2EQbZWjsBA9Z0Ado)

- [Glancier](https://aws.amazon.com/ko/glacier/faqs/)

## S3 Performance

- network

  ![](https://image.slidesharecdn.com/awsdatatransferservices-acceleratinglarge-scaledataingestintotheawscloud-160419202216/95/aws-data-transfer-services-accelerating-largescale-data-ingest-into-the-aws-cloud-17-638.jpg?cb=1461364067)

- [hash name](https://aws.amazon.com/ko/premiumsupport/knowledge-center/s3-bucket-performance-improve/)

## S3 Select

- Presto 기반의 sql 문으로 S3 내부의 데이터를 query
- 조건이 완료되면 종료 되어 성능 향상
- Athena 에서 내부적으로 사용중

## REFERENCE

- [Why you should move AWS S3 objects storage class from STANDARD to STANDARD_IA](https://medium.com/@Bits_Bytes/why-you-should-move-aws-s3-objects-storage-class-from-standard-to-standard-ia-266b6e836e0e)
- [S3 Cost](https://www.cloudhealthtech.com/blog/s3-cost-aws-cloud-storage-costs-explained)
- [S3 Price](https://aws.amazon.com/ko/s3/reduced-redundancy/)
- [S3 storage class](https://docs.aws.amazon.com/ko_kr/AmazonS3/latest/dev/storage-class-intro.html)
- [S3 Performance tip](https://aws.amazon.com/ko/blogs/aws/amazon-s3-performance-tips-tricks-seattle-hiring-event/)

https://docs.aws.amazon.com/ko_kr/AmazonS3/latest/dev/lifecycle-configuration-examples.html
