# Elastic Search

## 요약

![searchengine](https://irp-cdn.multiscreensite.com/3b198145/dms3rep/multi/desktop/Paid%26OrganicSearch.jpg)

- 검색엔진입니다!

![안녕](https://2runzzal.com/media/ckFJeFFUYnNpVXQ3S2VlMHMvVWdYZz09/thum.jpg)









## 특징에는 무엇이 있나요?

### Schema가 존재하지 않습니다

- 물론 처음부터 지정해주고 시작할 수도 있습니다 - 추후 *mapping* 에서 설명할 수 있습니다
- 없는 key - value가 올지라도 알아서 인덱싱 할 수 있습니다
- 다시말해, NoSql 기반입니다

### Multi-tenacy

- 한 Elastic search 서버 내에 여러게의 index를 지정하고, 이를 활용할 수 있습니다!
- 한 index내에서도 여러개의 Type을 또한 설정하여 저장할 수 있습니다!

![RDBMSvsES](https://cdn-images-1.medium.com/max/1600/1*CxsgFKMhXZXnRkDXQk4tKg.png)



### 확장성 및 유연성에서 강점

- [플러그인](https://www.elastic.co/guide/en/elasticsearch/plugins/6.4/index.html)을 통해서 기능을 확장할 수 있습니다!
- Analysis Plugin부터 Mapper Plugin까지



![은전한닢](https://image.slidesharecdn.com/dockerseoulmeetup4th-150906065135-lva1-app6892/95/elasticsearch-10-638.jpg?cb=1441524409)



기왕에 저희가 쓴 plugin에 대해서 간단히 짚고 넘어가자면...

- 한국어 형태소 분석기인 은전한닢 또한 플러그인 중의 하나

  - 기존의 영어, 숫자와 같은 1 Byte기반의 text와 달리 한글은 2 Byte 기반
  - 또한 한글의 구문이 영어와는 달라 형태소 분석또한 따로 지원이 필요
  - 분석하는 방법은 조금 어렵고, 필요 이상으로 깊이 들어가는 것 같아 제외합니다...
  - ~~[6.4에 새롭게 Nori](https://www.elastic.co/blog/elastic-stack-6-4-0-released?blade=tw&hulk=social)가 나왔다던데 시간되면 환경까지 setting해서 한번 써보고 싶어요 (잡담)~~



  ![dump...](https://media.giphy.com/media/oFRI4g517yWaI/giphy.gif)

### 분산 시스템을 사용

- 샤드와 노드를 사용해서 검색성능을 향상시킬 수 있습니다

![esdiagram](https://t1.daumcdn.net/cfile/tistory/2346E93C55EE676524)

#### 샤드 & 레플리카

- 하나의 index가 비대하게 커질 경, 검색 속도의 한계가 나타나게 됩니다
- index를 여러개의 Shard로 나눠 볼륨을 분할/확장 가능하게 만들어줍니다
- 오류 등으로 인해 사라질 경우를 대비하여 failover를 대비 해야함 - replica

#### 노드??

- ES 분산시스템을 구성하는 최소의 단위
- 하나 이상의 노드가 모인 것을 ES의 가장 큰 단위, 클러스터(cluster)로 부름
- 데이터를 저장하고, 인덱싱, 검색 작업에 참여합니



## 현재 사용중인 서비스

![AWS elastic search](http://139.59.2.72:2086/wp-content/uploads/2018/04/ElasticSearchIcon.png)

- [6.3 버전까지 지원](https://aws.amazon.com/ko/about-aws/whats-new/2018/08/amazon_elasticsearch_service_announces_support_for_elasticsearch_versions_5-6_and_6-3/)

  - Nori는 6.4버전부터 가능...

- [현재 위치 업그레이드 지원](https://aws.amazon.com/ko/about-aws/whats-new/2018/08/amazon_elasticsearch_service_now_supports_zero_downtime_in-place_version_upgrades/)

  - 가동 중지 시간없이 클러스터를 새버전으로 업데이트 가능

- [다수 플러그인을 native하게 지원](https://docs.aws.amazon.com/ko_kr/elasticsearch-service/latest/developerguide/aes-supported-plugins.html)

  - 은전한닢 포함

- ELK stack 지원

  - Elasticsearch, Logstash, Kibana

  ![elk](https://t1.daumcdn.net/cfile/tistory/99830B3359A038AC1F)