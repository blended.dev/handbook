# Lambda & API GateWay Deploy

## 배포의 종류

1. All-at-once

- 모든 트래픽은 이전 버전에서 새 버전에서 새 버전으로 즉시 이동

2. Blue / Green

- 프로덕션 트래픽을 처리하기전에 새 버전은 배포되고 테스트됨. 유효성 검사 한 후 모든 트래픽이 이전 버전에서 새 버전으로 즉시 변경

3. Canary / Linear

- 프로덕션 트래픽의 일부분은 새버전으로, 나머지는 이전 버전으로 보냄.
- 유효성 검사를 위해 일정 기간이 지나면 트래픽이 점진적으로 이동되거나 새 버전으로 완전히 이동.
  - 사용량을 늘리기 전에 새로운 버전에 대한 Health check 를 통해 오류를 줄인다.
  - 만약 오류가 발생하더라도 Roll Back 에 대한 부담이 적다

## Lambda

- Lambda 는 기본적으로 All-at-once 만 사용가능
- Version 과 Alias 를 구분해서 사용
  - Version 은 immutable 함
  - Alias 는 Version 을 가리키는 mutable 한 값
  - 둘 다 고유한 ARN 을 가짐
- [Lambda 에서 Canary 사용하기](https://aws.amazon.com/ko/blogs/compute/implementing-canary-deployments-of-aws-lambda-functions-with-alias-traffic-shifting/)
- `aws lambda update-alias --function-name myfunction --name myalias --function-version 2 --routing-config '{}'`

### CodeDeploy

- AWS CodeDeploy 는 Amazon EC2 인스턴스, 온프레미스 인스턴스 또는 서버리스 Lambda 함수로 애플리케이션 배포를 자동화하는 배포 서비스
- Lambda 에는 기본적으로 들어 있지않은 배포 기능 ( Canary / Linear ) 를 구성 할 수 있습니다.
  - Github 에 연결은 잘 되어있지만 Gitlab 은 지원 X
  - CodeDeploy 를 잘 사용할려면 CodePipeLine, CodeBuild 과 같은 서비스를 사용해야 함

## API Gateway

- Canary 배포가 가능
- ![](./image/1111.png)
- ![](./image/v2.png)
- ![](./image/v3.png)

## REFERENCE

- https://www.youtube.com/watch?v=uldwig_kUbE
- https://www.slideshare.net/AmazonWebServices/unlocking-agility-with-the-aws-serverless-application-model-sam
- https://www.slideshare.net/awskorea/deep-dive-in-servless-application-deployment-piljoong-kim
- https://docs.aws.amazon.com/ko_kr/codedeploy/latest/userguide/deployment-configurations.html
