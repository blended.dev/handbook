2018 년 5 월 11 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# [AWS Lambda](https://aws.amazon.com/lambda/)

AWS Lambda 의 기본 개념과 Preferences 를 살펴보도록 하겠습니다. Huiseoul 은 중국 서비스를 지원하므로 China(Beijing) Region 을 기준으로 합니다.

## Lambda?

* Event 기반의 Serverless Computing Service
* Event 기반?

  * 사용자가 지정한 Event 를 trigger 로 Lambda function 이 수행
  * ex)API Gateway, SNS, S3, DynamoDB 등

* [Serverless Computing Service](https://en.wikipedia.org/wiki/Serverless_computing)?

  * 기본적인 Computing Resource 관리는 서비스제공자(AWS)가 제공하여 사용자는 기능에 집중할 수 있음

---

## Preferences

Lambda Service Console 에서 볼 수 있는 환경설정을 간략히 설명합니다.

* Throttle : Lambda function 이 동시성 한도(`$ACCOUNT`)에 도달했을 경우 이 후에 들어오는 invocation 을 무시합니다.

  * 한 리전의 기본 Concurrent Excution Limit 은 1,000 입니다.
  * 추가 Resouce 가 필요할 경우 별도 요청할 수 있습니다. [Managing Concurrency 참고](https://docs.aws.amazon.com/lambda/latest/dg/concurrent-executions.html)

* [Qualifiers](https://docs.aws.amazon.com/lambda/latest/dg/versioning-aliases.html): Lambda function 은 Version 또는 Alias 를 가질 수 있습니다.

  * `Qualifier` option 을 지정하여 API 호출 시 특정 Version 또는 Alias 의 Lambda function 을 호출할 수 있습니다.
  * `routing_config` 명령으로 지정한 percentage 만큼만 특정 버전을 실행할 수 있도록 설정 가능합니다. ex) version(old): 90%, version(new): 10% 를 지정하여 new version 의 안정성 확인/확보 가능

* Execution role: [AWS IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html) 에서 생성한 권한을 지정할 수 있습니다.

  * role 에 따라 Lambda function 이 접근/실행 할 수 있는 범위가 달라집니다.

* Basic settings(Memory & Timeout)

  * Lambda 는 function 의 실행 시간 및 자원 사용량에 따라 과금됩니다.
  * 따라서 Memory 사용량이 높거나 Timeout 제한이 길 경우 과잉 과금될 수 있으므로 function 의 역할에 맞는 적절한 자원을 할당합니다.

* Network(VPC)

  * 특정 VPC 망 내에서만 사용할 수 있는 서비스들이 있습니다. (ex. Redshift Data Warehouse, ElastiCache 등)
  * VPC 를 선택하고 적절한 서브넷과 보안그룹을 지정하면 Lambda 에서 VPC 망 내 서비스를 활용할 수 있습니다. [참고](https://aws.amazon.com/ko/blogs/korea/intro-to-vpc-aws-lambda/)

* [Debugging and error handling](https://docs.aws.amazon.com/lambda/latest/dg/retries-on-errors.html)
  * function 실행에 실패한 경우 DeadLetterQueue 를 활용하여 실패한 요청을 확인할 수 있습니다.
  * error 발생 시 Lambda 가 기본적으로 2 회 재시도 후 실패한 요청에 대해 DLQ 에 추가합니다.
  * 현재 Lambda 는 `SNS`, `SQS` 두 가지 방식을 지원합니다.
  * **_참고_**: Asynchronous invocation 일 경우만 동작합니다. function 호출 시 request 의 Invocation Type 을 `Event`로 지정하여 호출합니다.

---

## [API](https://docs.aws.amazon.com/ko_kr/lambda/latest/dg/API_Invoke.html)

### Request

* `FunctionName`: functionName | ARN
* [InvocationType](https://docs.aws.amazon.com/lambda/latest/dg/retries-on-errors.html): 실행타입
  * `RequestResponse`: Synchronous, default (return 200)
  * `Event`: Asynchronous (return 202)
    * Lambda 자체 대기열을 통해 invocation 명령이 들어온 순서대로 실행됩니다.
  * `DryRun`: for Test, 실행 권한과 입력값만 검증하고 실행은 하지 않음 (return 204)
* `LogType`: 실행 후 Log return 설정 (RequestResponse type 에만 유효)
  * None: 미설정
  * Tail: 로그의 마지막 일부만 반환(4KB)
* `Qualifier`: 실행할 Lambda function 의 Version or Alias 지정
* `Payload`: data payload (event trigger 마다 필요한 attributes 가 다름)

### Response

* StatusCode: HTTP StatusCode
* FunctionError: ErrorType
* LogResult: Request.LogType 설정에 따른 Log 결과
* Payload: Lambda function 의 return value

---

## [Monitoring](https://docs.aws.amazon.com/lambda/latest/dg/monitoring-functions-metrics.html)

Lambda 는 CloudWath 감시를 통해 Monitoring 서비스를 지원합니다.
각 항목별 내용과 모니터링 방법을 설명하겠습니다.

![Lambda Monitoring](https://cdn.huiseoul.com/lambda_monitoring_01.png)

* Invocations (단위: count)
  * Lambda function 에 들어온 요청 수 (성공, 실패 모두 포함)
* Duration (단위: ms)
  * 기간 내 각 function 당 invocation 발생시점(function call) 부터 function 수행을 마칠 때 까지 걸린 최대/평균/최소 시간을 표기
* Error (단위: count)
  * 정상적으로 마쳐지지 않은 요청 수(function 수행중 error 포함)
* Throttles (단위: count)
  * Concurrency Limit 을 넘어선 요청으로 강제종료된 요청 수
* Iterator age (단위: ms)
  * Stream 기반의 event 에만 해당
* DLQ errors (단위: count)
  * DLQ 이벤트를 정상적으로 발생시키지 못한 요청 수
  * 에러가 난 모든 케이스가 아니라 에러가 있지만 DLQ 에 쓰지 못한 경우
