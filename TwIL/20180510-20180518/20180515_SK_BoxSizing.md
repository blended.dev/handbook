# CSS box-sizing

## 개념

- HTML 문서에서는 각 요소를 박스의 형태로 그리는데 `margin`, `border`, `padding`, `content`의 순서로 박스를 여러 단계로 그리는 방식으로 되어 있습니다
- 이때 가로나 세로 크기를 지정하면(예를 들어, `width: 100px;`) 이것을 각 계층의 박스에 어떻게 적용할지를 결정하는 것이 `box-sizing` 속성입니다
    - `contet-box`로 지정하면 `width: 100px;`은 `content`의 크기를 `100` 만큼 지정하고 그 이외에 `padding` 및 `border`값은 이 값에 더해지게 됩니다. 따라서 `border-width: 10px`, `padding: 10px`를 지정하면 최종적인 요소의 너비는 `10 + 10 + 100 + 10 + 10` 으로 `140px`이 됩니다.
    - `border-box`로 지정하면 `width: 100px;`는 `content` 및 `padding`, `border`를 포함한 크기를 의미합니다. 따라서 최종 요소의 너비는 `100px`이 되고 `content`는 `100 - 10 - 10 - 10 - 10`으로 `60px`가 됩니다.
- 각 브라우저의 기본값은 `content-box`이며 ReactNative에서도 `content-box`를 가정하고 크기를 정합니다(RN에서는 `border-box`로 변경할 수 없음).

## 예시

- [content-box와 border-box를 구현한 간단한 예시](https://codepen.io/sunkibaek/pen/XqBqVQ)

## 참조

- [CSS 박스 모델 입문](https://developer.mozilla.org/ko/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model)
- [box-sizing](https://developer.mozilla.org/ko/docs/Web/CSS/box-sizing)
