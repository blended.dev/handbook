# Cognito

---

# Cognito 란

![Imgur](https://i.imgur.com/pSqRNi8.png)



Cognito 는 Userpool 과 Federated Identities 로 2개의 서비스를 관리하는 기능입니다. 

---
# Cognito 장점

* User Pools
  Facebook 이나 Google 같은 외부 Userpool 뿐만 아니라 SAML을 제공하는 제 3 의 Userpool , Cognito Userpool을 통해 다양한 방법으로 접근할 수 있도록 합니다.

* Amazon Cognito Sync

  Cognito Sync를 이용하여 클라이언트사이의 사용자 데이터를 동기화 할 수 있습니다. 

* Federated Identities

  다양한 방식으로 UserPool을 적용하고 통합적으로 관리할 수 있습니다.

* Easy
  위 와 같은 기능을 적은 코드로 구현할 수 있습니다.

---

# Cognito Structure
![Cognito Structure](https://image.slidesharecdn.com/gettingstartedwithcognitouserpools-160921230415/95/getting-started-with-cognito-user-pools-september-webinar-series-28-638.jpg?cb=1474499316)

---

# Cognito Validation Flow

![cognito flow](https://docs.aws.amazon.com/cognito/latest/developerguide/images/amazon-cognito-ext-auth-basic-flow.png)

Cognito는 그룹을 이용하여 권한을 나눌 수 있습니다. 권한은 AWS IAM을 이용하여 부여할 수 있습니다. IAM은 AWS STS를 이용하여 임시 발급되며 Unauthorization도 정의할 수 있습니다.



---

# Cognito Sign Up example
![Imgur](https://i.imgur.com/A6Cl4Mr.png)

[amazon-cognito-identity-js](https://github.com/amazon-archives/amazon-cognito-identity-js) 를 이용하여 예제를 구현 하였습니다.

```js
// sign up
const email = this.state.email.trim();
    const password = this.state.password.trim();
    const attributeList = [
      new CognitoUserAttribute({
        Name: "email",
        Value: email
      })
    ];
    userPool.signUp(email, password, attributeList, null, (err, result) => {
      if (err) {
        console.log(err);
        return;
      }
      console.log("user name is " + result.user.getUsername());
      console.log("call result: " + result);
    });
```



```js
// login
var authenticationData = {
      Username: this.state.email,
      Password: this.state.password
    };
    var authenticationDetails = new AuthenticationDetails(authenticationData);
    console.log(authenticationDetails);
    var userData = {
      Username: this.state.email,
      Pool: userPool
    };
    var cognitoUser = new CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function(result) {
        console.log("access token + " + result.getAccessToken().getJwtToken());

        AWS.config.region = appConfig.region;

        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: appConfig.IdentityPoolId
        });

        AWS.config.credentials.refresh(error => {
          if (error) {
            console.error(error);
          } else {
            console.log("Successfully logged!");
          }
        });
      },

      onFailure: function(err) {
        alert(err.message || JSON.stringify(err));
      }
    });
```

---

# Cognito Pricing

![Imgur](https://i.imgur.com/m95k7LB.png)



적은 유저 사용량을 가진 프로젝트일 경우 매우 효율적으로 사용이 가능하지만 50000 ~ 는 가격이 부담이 됩니다. 하지만 매우 많은 사용자를 관리하는 경우라면 긍정적으로 검토할 수 있습니다.

---

# Cognito with API gateway
![Cognito with API gateway](https://camo.githubusercontent.com/793681e54a8c3ebdaac694bcba8eb56e1a9f7d22/68747470733a2f2f6432393038713031766f6d7162322e636c6f756466726f6e742e6e65742f316236343533383932343733613436376430373337326434356562303561626332303331363437612f323031372f30362f30372f696d6167653030392e706e67)

---

# Cognito in huiseoul?

* ID Password 방식이 아님

  Lambda로 auth challenge 구현

* Cognito에서 지원하지 않는 인증방법
  Lambda로 인증절차를 구현

* 유저가 AWS Role을 이용하지 않음

 ### 발표 이후 추가

* admin 의 경우 ID Password 방식 ,적은 사용량 , AWS Role 을 사용하는 경우가 있어 admin의 경우 긍정적으로 검토가 가능합니다.

---