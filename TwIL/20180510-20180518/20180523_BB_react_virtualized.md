2018 년 5 월 23 일 @bobeenlee

# React Virtualized

## 사용법

### 무한 스크롤
[Documentation](https://github.com/bvaughn/react-virtualized/blob/master/docs/creatingAnInfiniteLoadingList.md)을 제공해준다. <br/>
무한 스크롤을 하기위해선 기본 List 가 필요하고 상단에 InfinteLoader 를 감싸주어여한다.<br/>
width를 화면사이즈가 맞추기 위해서 AutoSizer를 사용하였다. <br/>

* 대략적인 사용방법은 다음과 같습니다.
```javascript
import { AutoSizer, InfiniteLoader, List } from "react-virtualized";
<InfiniteLoader
    isRowLoaded={isRowLoaded}
    loadMoreRows={loadMoreRows}
    rowCount={rowCount} // required rowCount
>
    {({ onRowsRendered, registerChild }) => (
    <AutoSizer disableHeight>
        {({ width }) => (
        <List
            width={width}
            height={height} // required height
            ref={registerChild}
            onRowsRendered={onRowsRendered}
            rowCount={rowCount} // required rowCount
            rowHeight={rowHeight} // required rowHeight
            rowRenderer={rowRenderer} // required rowRenderer
        />
        )}
    </AutoSizer>
    )}
</InfiniteLoader>
```

### 데이터가 많은 정렬 테이블
[Documentation](https://github.com/bvaughn/react-virtualized/blob/master/docs/tableWithNaturalSort.md) <br/>
* 대략적인 사용방법은 다음과 같습니다.
```javascript
import { Column, Table, AutoSizer } from 'react-virtualized';

const columns = [
    {
        dataKey: "index", label: "Index", cellDataGetter: ({ rowData }) => rowData.index, disableSort: false, width: 70,
    },
    { dataKey: "name", label: "Name", disableSort: false, width: 90 },
    {
        dataKey: "random",
        label: "Description",
        disableSort: true, cellRenderer: ({ cellData, rowData }) => <div>{cellData}<button>{rowData.name}</button></div>, width: 200, flexGrow: 1
    },
    {
        dataKey: "date",
        label: "Date",
        disableSort: false, cellRenderer: ({ cellData }) => cellData.format("YYYY-MM-DD"), width: 200
    }
];

 <AutoSizer disableHeight>
    {({ width }) => (
        <Table
            width={width}
            headerHeight={headerHeight} // required headerHeight
            height={height} // required height
            noRowsRenderer={() => <div>No Render</div>}
            rowHeight={rowHeight} // required rowHeight
            rowGetter={rowGetter} // required rowGetter
            overscanRowCount={20}
            rowCount={rowCount} // required rowCount
            sort={renderSort}
            sortBy={sortByKey}
            sortDirection={sortDirection}
        >
            {_.map(columns, column => (
                <Column key={column.dataKey} {...column} />
            ))}
        </Table>
    )
    }
</AutoSizer >
```

## 활용방안

* 무한 스크롤
  * 채팅창, 타임라인, [캘린더](https://github.com/clauderic/react-infinite-calendar) 등등
* 정렬 테이블
  * 어드민 or 관리 테이블, 많은 데이터가 요구될 시

## 예제 소스

* https://github.com/BoBinLee/react-virtualized-example

## 참조

* https://github.com/bvaughn/react-virtualized
