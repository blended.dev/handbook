오토마타 이론을 기반으로 작성된 상태 관리 라이브러리 입니다.

오토마타를 사용하면,  복잡한 비동기 로직에 따라 UI 를 랜더하는 영역을 분리하고 JSX 템플릿에 작성되는 코드를 간결하게 해주는  장점이 있다고 생각 합니다.

적용 하는 방법은 매우 간단 합니다.

HOC(High Order Component) 방식으로 작성해 주면 됩니다.

    withStateMachine(statechart)(App)

stateChart 를 작성하고  Action, 또는 State 를 컴포넌트를 작성하여 사용하면 됩니다.

토글 상태를 예로 들어보겠습니다.
```js
    class App extends React.Component {
    
      state = {
        toggle: false,
      }
    
      handleClick = () => {
        this.setState({ toggle: !state.toggle });
      }
    
      render() {
        const { toggle } = this.state;
        return (
          <div>
            <button onClick={this.handleClick}>NEXT</button>
              {
    					   toggle ? <div>Hello, A</div> : <div>Ciao, B</div>
    				  }
          </div>
        )
      }
    }
```
이런 상태를 automata 를 통해 변경해 보겠습니다.
```js
    const statechart = {
      initial: 'a',
      states: {
        a: {
          on: {
            NEXT: 'b',
          },
          onEntry: 'sayHello',
        },
        b: {
          on: {
            NEXT: 'a',
          },
          onEntry: 'sayCiao',
        },
      },
    }
    
    class App extends React.Component {
      handleClick = () => {
        this.props.transition('NEXT')
      }
    
      render() {
        return (
          <div>
            <button onClick={this.handleClick}>NEXT</button>
            <Action is="sayHello">Hello, A</Action>
            <Action is="sayCiao">Ciao, B</Action>
          </div>
        )
      }
    }
    
    const WithApp = withStateMachine(statechart)(App)
```
단순 토글을 봤을땐, 가독성에 큰차이가 없어 보이지만, 조금더 명시적이기도 하고

삼항식이 많아질수록, 진가를 발휘 할 것 같습니다.

출처: [리액트 암스테르담](https://www.youtube.com/watch?v=sZg3DoTfHLQ&t=260s), [라이브러리 레파지토리](https://github.com/MicheleBertoli/react-automata)