2018 년 9 월 27 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# Cron Expression

## Cron?

Unix 계열의 OS 에서 사용되는 시간기반의 Job Scheduler

## Cron Expression

- Cron Job Scheduler 의 정규 표현식
- 총 5-7 개의 필드로 구성 (OS 환경에 따라 다름, [표 1]의 `Required` 참고)
- space 로 각 필드를 구분

| Field        | Required | Allowed Value   | Allowed special characters    | Remarks                                                        |
| :----------- | :------- | :-------------- | :---------------------------- | :------------------------------------------------------------- |
| Seconds      | No       | 0-59            | `*`, `,`, `-`                 |                                                                |
| Minutes      | Yes      | 0-59            | `*`, `,`, `-`                 |                                                                |
| Hours        | Yes      | 0-23            | `*`, `,`, `-`                 |                                                                |
| Day of month | Yes      | 1-31            | `*`, `,`, `-`, `?`,`L`,`W`    | `?`,`L`,`W` only in some implementations                       |
| Month        | Yes      | 1-12 or JAN-DEC | `*`, `,`, `-`                 |                                                                |
| Day of week  | Yes      | 0-6 or SUN-SAT  | `*`, `,`, `-` , `?`, `L`, `#` | `?`, `L`, `#` only in some implementations                     |
| Year         | No       | 1970-2099       | `*`, `,`, `-`                 | This field is not supported in standard/default implmentations |

[표 1]. cron expression

### Special Characters

- `*`: 모든 값
- `,`: 특정 값의 리스트를 구분할 때
  - ex) "MON,WED,FRI"
- `-`: 범위
  - ex) 2018-2100
- `?`: 해당 항목을 사용하지 않음
- `L`: last
  - 일에서 사용하면 마지막 요일
  - 요일에서 사용하면 마지막 요일 ex) 마지막 금요일 5L
- `W`: 가장 가까운 평일
  - 15W 는 15 일에서 가장 가까운 평일
- `#`: A 째주의 B 요일 (B#A)
  - ex) 5#3 세번째 금요일
- `/`: 시작시간과 반복 간격
  - ex) second 0/15 면 0 초에서 시작해서 15 초 간격
  - ex) minutes 5/10 면 5 분에 시작해서 10 분 간격으로 동작

### 예!

| Expressions         | Means                                                                              |
| :------------------ | :--------------------------------------------------------------------------------- |
| 0 0 12 \* \* \*     | 매일 12 시에 실행                                                                  |
| 0 30 23 \* \* \*    | 매일 저녁 11 시 30 분에 실행                                                       |
| 0 \* 1 \* \* \*     | 매일 오전 1 시 0 분 ~ 59 분까지 매분 실행                                          |
| 0 3/10 \* \* \* \*  | 매일 10 분마다 실행하되 3 분일 때 실행 (3 분, 13 분, 23 분, ...)                   |
| 0 0 23 ? \* MON-FRI | 월~금요일 저녁 11 시에 실행                                                        |
| 0 0 8 ? \* 6L       | 매월 마지막 금요일 아무날이나 8 시에 실행 (요일은 0 부터 시작되며 시작일은 일요일) |

[표 2]. cron expression 예시

- [만들어 주는 애도 있다](http://www.cronmaker.com/)
