# 요약

Cosmochain engineer의 workflow를 공유합니다.

## Workflow의 중심모델

- Issue
- MR(Merge Request)

Workflow는 issue와 MR을 중심으로 합니다. Issue는 해야하는 일에 대한 정의와 계획이고, MR은 실제로 한 일을 팀과 공유하는 단위입니다. 모든 일은 issue로 시작해서 MR로 끝납니다.

## Issue

Issue는 해야하는 일에 대한 정의와 계획입니다.

### Issue의 단위

Issue의 단위가 클수록 MR이 커지기 쉽고, 이는 review를 어렵게 합니다. 작업이 길어지면 작업을 시작할 때의 code base가 변경될 확률이 높기 때문에, merge가-즉 issue의 해결이- 어려워집니다. Issue는 하루에(길어도 이틀) 작업할 수 있을 정도의 분량으로 정리해야 합니다. 그 이상의 소요될 경우, 중심 issue를 기준으로 세부 issue를 나눠서 생성합니다.

Issue는 workflow의 중심 모델입니다. 가장 좋은 경우는 issue가 하루 안쪽의 작업량을 가진 하나의 MR로 끝나는 경우입니다.

### Issue의 생성

Issue는 모든 일의 시작입니다. 모든 작업 전 issue를 먼저 생성해주세요. Issue의 내용은 각 repo의 template을 참고합니다.

Issue 는 완전한 문장으로, 존댓말로 작성해주세요. 버그의 경우, 이슈의 제목은 현상을 설명하는 것이어도 됩니다. 이를 제외하고는 해야하는 일을 명확하게 작성합니다. 누구나 제목을 보고도 무슨 일인지 알 수 있도록이요. 상세한 내용 없이 '개선한다', '변경한다' 등의 추상적인 표현을 쓰지 않도록 주의합니다.

Issue는 누구든 생성할 수 있습니다. 스프린트 미팅 후 engineer 미팅을 거쳐 간단히 이슈를 생성하고, 이후 각 engineer가(혹은 manager가) issue의 세부사항을 작성하는 것이 일반적인 흐름입니다

Issue 의 생성과 작업의 시작을 동시에 하지 않습니다(아래, Issue 의 작업 시작 section 참고). Issue 내용이 작업을 시작할 수 있는 수준이라면, `Status: Ready to be taken` label을 붙입니다. 이때, 해당 이슈에 '이슈 작성을 완료했다'는 코멘트를 작성합니다. 작업할 이슈가 있다는 것을 모두에게 알리는 것은 물론, 이슈가 작업하기에 충분한 지 스스로 점검한다는 의미가 있습니다.

### Issue의 작업 시작

Issue를 가져갈 때는, assignee에 자신을 추가하고, `Status: In progress` label을 추가합니다. Issue를 처리하기 위해 계획하는 바를 코멘트에 간단히 정리합니다. Issue가 close되기까지 대략 어느 정도가 걸릴 지를 추산해서 estimation을 기록합니다.

작업을 시작하는 이슈가 더 큰 이슈에 포함되는 경우, 해당 이슈의 assignee에도 자신을 추가합니다. 작업 중 발생한 결정사항은 기록을 위해 코멘트로 기록합니다.

### Issue의 close

Issue는 해당 issue가 해야하는 일이 완료되었을 때, close 합니다. 보통은 issue를 해결하기 위한 MR이 merge될 때 입니다.

## MR(Merge Request)

### MR의 단위

MR은 팀원이 리뷰하기에 적절한 단위를 유지합니다. MR이 길어질 경우, MR을 나눕니다. 대략 하루 정도의 작업량으로 제한합니다.

### MR의 생성

MR은 이슈를 충분히 분석한 후 생성합니다.

작업할 때는 최대한 작은 단위로 많은 commit을 하는 습관이 좋습니다. 하지만, 모든 commit이 올라올 경우 리뷰가 불편할 수도 있으므로 remote로 push하기 전, rebase를 통해서 commit history를 정리하는 것을 추천합니다. Commit은 모두가 본다는 생각을 하고, 단위, 제목, 코멘트에 공을 들여야 합니다. 단, 이미 remote에 push한 history는 변경하지 않습니다([GitLab flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)문서 읽기를 권합니다).

MR의 내용은 template에 맞게 생성합니다. 작업을 완료했거나 리뷰를 요청할 때, template을 적용합니다. 이 때 `remove source branch` option을 활성화합니다.

MR을 생성했다는 것은 작업을 시작했다는 의미이므로, assignee에 자신을 배정합니다. MR은 labeling을 강요하지 않습니다. Project 상황은 issue를 통해서 확인하려고 합니다. Issue와 비슷하게, 해당하는 MR을 완료하기까지 예상되는 시간을 estimation으로 기록합니다. Weight 역시 기록합니다. Weight는 일의 난이도를 1~9로 나눴을 때 짐작되는 난이도(최하 1, 최고 9)를 기록합니다.

대략의 계획을 MR의 설명이나 코멘트로 기록합니다.

### MR의 상태

작업중인 MR의 경우 WIP 말머리를 붙입니다.([참고 링크](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html))

### MR의 리뷰

MR의 작업이 완료되었을 경우, 리뷰를 요청합니다. 리뷰는 기본적으로 subteam(frontend, backend, blockchain)원에게 요청합니다. 단순히 `Approve`를 누르는 것은 가장 낮은 수준의 리뷰입니다. 좋은 부분, 궁금한 부분, 아쉬운 부분을 남기는 것이 주목적입니다. 그러니 가능한 작은 단위로 일을 나누어 가능한 빨리 리뷰를 요청하고, 이에 대해 의견을 나누는 것을 추천합니다. MR이 길거나 설명이 불충분해서 리뷰를 진행하기 어려울 경우, 작업자에게 수정을 요구해야 합니다. 의견이 갈라져 일치가 어려울 경우 manager를 소환합니다.

리뷰로 인해 commit이 추가된 경우 다시 리뷰를 받아야 합니다.

WIP 상태에 있지만, 작업이 예상 외로 길어질 경우 작업자는 중간 리뷰를 요청할 수 있습니다.

### MR의 merge

Approve를 받았고, CI를 통과한 MR은 master branch로 merge할 수 있습니다. 작업자는 리뷰가 끝난 경우 MR을 merge 합니다. Source branch가 삭제 되었는지 확인합니다. MR의 merge로 issue가 마무리된 경우, 해당 issue를 close하는 것까지 진행합니다.
